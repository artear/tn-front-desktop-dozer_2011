//Grid
DD_belatedPNG.fix('form.search .submit');
DD_belatedPNG.fix('.num-comments span, .views span');
//Header
DD_belatedPNG.fix('.more a, .tweet .wrapper .bird, #header .tweet, #sub-menu .close a, #header #header-top a');
//Content top
DD_belatedPNG.fix('span.ribbon');
//Rio de noticias
DD_belatedPNG.fix('figure .play');
//Sidebar
DD_belatedPNG.fix('.sidebar #opinion .content, #traffic-widget .content .item .circle, .twtr-tweet-text b');
//Footer
DD_belatedPNG.fix('#tnlogo, .iconosur');
//Varias
DD_belatedPNG.fix('#movistar-ad .logo, #movistar-ad .legal, figure figcaption, .tooltip, .share li.link a, .subscribe li.link .rss');
//add a class to the body
DD_belatedPNG.fix('.weather-icons-small img, .weather-icons img, #content-top .form-inner form .input-state, .comunity form.config-accounts .account, #rss .content .icon');
/*
var body = document.getElementsByTagName("body");
body[0].className += ' dd_png';
*/