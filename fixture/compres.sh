mkdir -p tmp/css

echo 'compresing css...'
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type css --line-break 200 -o tmp/css/00_reset.css  content/css/reset.css
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type css --line-break 200 -o tmp/css/10_fonts.css  content/css/fonts.css
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type css --line-break 200 -o tmp/css/20_coda-slider.css  content/css/coda-slider-2.0.css
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type css --line-break 200 -o tmp/css/30_style.css  content/css/style.css

java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type css --line-break 200 -o content/css/ie-min.css  output/css/ie-min.css

cat tmp/css/* > output/css/fixture-min.css
rm -rf tmp/css
echo 'css compresed and copied to output'

echo 'compresing js...'
mkdir -p tmp/js

java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/js/10_jquery-coda-slider-2.0.js content/js/plugins/jquery-coda-slider-2.0.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/js/20_jquery-easing.1.3.js content/js/plugins/jquery-easing.1.3.js

cat tmp/js/* > content/js/fixture-min.js
cp content/js/fixture-min.js output/js/fixture-min.js
rm -rf tmp/js
echo 'jss compresed and copied to output'
