function time_ago(dateString){
	var rightNow = new Date();
	var then = new Date(dateString);
	
	if (head.ie) {
	  // IE can't parse these crazy Ruby dates
	  then = Date.parse(dateString.replace(/( \+)/, ' UTC$1'));
	}

	var diff = rightNow - then;
	
	var second = 1000,
	    minute = second * 60,
	    hour = minute * 60,
	    day = hour * 24,
	    week = day * 7;
	
	if (isNaN(diff) || diff < 0) { return ""; } // return blank string if unknown
	if (diff < second * 2) { return "recién"; } // within 2 seconds
	if (diff < minute) { return "hace cerca de " + Math.floor(diff / second) + " segundos"; }
	if (diff < minute * 2) { return "hace cerca de 1 minuto"; }
	if (diff < hour) { return "hace cerca de " + Math.floor(diff / minute) + " minutos"; }
	if (diff < hour * 2) { return "hace cerca de 1 hora"; }
	if (diff < day) { return  "hace cerca de " + Math.floor(diff / hour) + " horas"; }
	if (diff > day && diff < day * 2) { return "ayer"; }
	if (diff < day * 365) {
		return "hace cerca de " + Math.floor(diff / day) + " días";
	} else {
		return "hace más de un año";
	}
	
}

function get_tweet(){
	var url = '/json';
	//url = 'http://twitter.com/statuses/user_timeline.json?screen_name=bebemusictw';
	
	var ajax = $.ajax({
		  url: url,
		  dataType: 'json',
		  cache: false
		});
	
	ajax.error(function(){
		$('#last-tweet .loading').fadeOut('slow');
	});
	
	ajax.success(function(data){
        var tweet = data[0].text;
        
        var img = data[0].user['profile_image_url'];
        img = img.replace('normal.jpg', 'bigger.jpg');
        $('#last-tweet img').attr('src', img).load();
        
        // process links and reply
        tweet = tweet.replace(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig, function(url) {
            return '<a href="'+url+'">'+url+'</a>';
        }).replace(/B@([_a-z0-9]+)/ig, function(reply) {
            return  reply.charAt(0)+'<a href="http://twitter.com/'+reply.substring(1)+'">'+reply.substring(1)+'</a>';
        });
        
        $('#last-tweet .tweet p').html('@' + data[0].user['screen_name'] + ': ' + tweet);
        
        $('#last-tweet .time-ago').html(time_ago(data[0].created_at));
        
        $('#last-tweet .error').remove();
        
        $('#last-tweet .loading').fadeOut('slow', function(){ $(this).remove() });
	});
	
}

function open($li){
	var h = $li.find('.content').outerHeight();
	$li.animate({height: '+=' + h }, 'slow', function(){$li.addClass('open')});	
}

$(document).ready(function() {
	
	get_tweet();
	
	$('#last-tweet .try-again').click(function(){
		$('#last-tweet .loading').fadeIn('fast');
		get_tweet();
		return false;
	});
	
	$('#more-info .toggle').click(function(){
		var $li = $(this).closest('li');
		var $ul = $(this).closest('ul');
		
		if ($ul.find('.open').length == 1){
			//close
			var $o = $ul.find('.open');
			$o.animate({height: '40' }, 'fast', function(){
													if ($li.hasClass('open') == false){
														open($li);
													}	
													$o.removeClass('open')
												});
		} else {
			open($li);
		}

	})
	
	
});