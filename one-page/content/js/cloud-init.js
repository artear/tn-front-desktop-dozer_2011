$(document).ready(function() {
    $('.hd').tooltip({tipClass: 'tooltip-hd',offset: [-5,10],effect:'fade',position: 'center right'});
    
    switch ($('body').data('page'))	{
    	case 'home':
	        var $c = $('#carousel-twitter');

          tw_carousel({'id': 'twitbox', 'type': 'search', 'search': $c.find('.search').data('search')});

	        $('#carousel-twitter').jCarouselLite({
	          btnNext: '#carousel-twitter .next',
	          btnPrev: '#carousel-twitter .prev',
	          visible: 3,
	          circular: false,
	          speed: 1000,
	          dynamic: true,
	          dyn_item_size: 315,
	          dyn_length: 20
	        });
	        
	        cplayer('#vivo .cplayer');  

    		break;

    	case 'nota-interna':
    		  $('a.icon-print').click(function () {
    			  window.print();
    			  return false;
    		  });
    		  
    		  h = $('#new .entry-content').height() - $('#tw-periodistas h3').outerHeight(true) - $('#tw-periodistas .twsb-bck').outerHeight(true) - 5; 
					
    		  if ( h < 330 )
    		  {
    			  h = 330;
    		  }
    		  
    		  
			  twlist = new TWTR.Widget({
			    'id': 'twpress',
			    'creator': 'tn',
			    'version': 2,
			    'type': 'search',
			    'search': $('#twpress').data('search'),
			    'interval': 5000,
			    'title': '',
			    'subject': '',
			    'width': 310,
			    'height': h,
			    'theme': { 
			    	'shell': { 'background': '#fff', 'color': '#444' },
			    	'tweets': { 'background': '#ffffff', 'color': '#444444', 'links': '#0056C3' }
			    },
			    'features': { 
			    	'scrollbar': true, 
			    	'loop': true, 
			    	'live': true, 
			    	'hashtags': true, 
			    	'timestamp': true, 
			    	'avatars': true, 
			    	'toptweets': true, 
			    	'behavior': 'default'}
			  });
    		  
    		  twlist.render();
			  
			  setTimeout('twlist.start()', 3500);
    		break;		    		
    }
    
 });    	