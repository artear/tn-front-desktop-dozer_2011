echo 'Init vars...'

css="output/css/"
cssclima="${css}clima/"
tmp="tmp/"
tmpcss="${tmp}css/"
tmpclima="${tmp}css/clima/"

echo 'Compresing css..'
mkdir -p tmp/css

#test

java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}00_reset.css  ${css}reset.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}05_core-layout.css  ${css}core-layout.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}10_style.css  ${css}style.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}20_fonts.css  ${css}fonts.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}30_coda-slider-2.0.css  ${css}coda-slider-2.0.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}40_twitter-widget.css  ${css}twitter-widget.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}50_cplayer.css  ${css}cplayer.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}55_dplayer.css  ${css}dplayer.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}70_fancybox.css  ${css}fancybox.css
#java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}60_ie.css  ${css}ie.css
#java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}70_moviles.css  ${css}moviles.css
#java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}80_weather-ie.css  ${css}weather-ie.css
#java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}90_weather-style.css  ${css}weather-style.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpcss}99_pulse.css  ${css}pulse.css

echo 'Especiales'
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}tanguito-min.css ${css}tanguito.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}stones-min.css ${css}stones.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}jjoo-min.css ${css}jjoo.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}soda-min.css ${css}soda.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}pepsimusic-min.css ${css}pepsimusic.css

cat ${tmpcss}* > ${css}core-min.css

mkdir -p tmp/css/tp
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o tmp/css/tp/10_site-tp.css  ${css}site-tp.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o tmp/css/tp/20_iframe-fixture.css  ${css}iframe-fixture.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o tmp/css/tp/30_menu-fixture.css  ${css}menu-fixture.css
cat tmp/css/tp/* > ${css}site-tp-min.css

java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}site-tn-min.css  ${css}site-tn.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}site-tnm-min.css  ${css}site-tnm.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}site-tnf-min.css  ${css}site-tnf.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}site-tnylagente-min.css  ${css}site-tnylagente.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}ckeditor-min.css  ${css}ckeditor.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}print-min.css  ${css}print.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}ie-min.css  ${css}ie.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}publish-min.css  ${css}publish.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${css}admin-min.css  ${css}admin.css

cat ${css}admin-min.css >> ${css}site-tnylagente-min.css

echo 'Clima'
mkdir -p tmp/css/clima

java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpclima}00_reset.css  ${cssclima}reset.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpclima}10_fonts.css  ${cssclima}fonts.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpclima}20_style.css  ${cssclima}style.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpclima}30_twitter-widget.css  ${cssclima}twitter-widget.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o ${tmpclima}40_jquery-ui-custom.css  ${cssclima}blitzer/jquery-ui-custom.css

java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o output/css/weather-ie-min.css  ${cssclima}ie.css

cat tmp/css/clima/* > ${css}weather-min.css

echo 'Fixture'
mkdir -p tmp/css/fixture

java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o tmp/css/fixture/00_reset.css  ${css}reset.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o tmp/css/fixture/10_iframe.css  ${css}iframe-fixture.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o tmp/css/fixture/20_menu.css  ${css}menu-fixture.css

#java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o output/css/weather-ie-min.css  ${cssclima}ie.css

cat tmp/css/fixture/* > ${css}fixture-min.css

