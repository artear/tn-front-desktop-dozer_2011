30/05/2012

al bloque de registarse se le agrego un id

23/05/2012

tnylagente
perfil-vivo, se quito el section de cada comentario por un div
el avatar es de 48x48
se agrego la clase clearfix a user-meta
se englobaron los comentarios en un div class content

13/4/2011

- se crearon las clases size-1, size-2, size-3, size-4, size-5 para el título de la home
- se actualizó el js init.js con código para las voloraciones de las notas y se actualizó
  el html de valoración de notas con un contenedor más
- se crearon inicializadores de js para activar distintas widgets en paginas. estos vana estar en TN.js
  el primero es TN.setup_nota_interna()
- cuando se pase a pre o producción tener en cuenta de ajustar el APPID de en js/libs/tn.js linea 15 sino no va a andar la valoracion de notas
- se corrigio el tag <a> de los avatares de los comentarios de la nota interna
- se agrego la clase "first" al primer <li> de #latest-news de la nota interna
- encuesta y latest new, se le agrego un <div class="content"> y se quito la clase "content" del ul
- se modifico el loginbox de la nota interna (tanto para logged-in como para not-logged-in)
- se cambio el html de la respuesta de la encuesta


14/4/2011

- se modificó el html y los links al pie de cada uno de los comentarios en la nota interna
- se agrego la clase "clearfix" a los comentarios de la nota interna
- se quito la clase "clearfix" al <ul> del review
- se agrego el id "poll" a la encuesta del sidebar (cuando se pregunta)
- transito, agrege el html de tw y fb debajo de la publicidad de movistar
- transito, quite el h1

15/4/2011

- se agrego el js para la votacion del sidebar
- se modifico el <span class="play">Play</span> de todos lados
- se modifico clima, casi toda la pagina
- quite un spam de los resultado de busqueda

18/04/2011

- comentarios de la nota, el titulo tiene un <h2> en ves de <h1>
- comentarios de la nota, el comentario tiene un <h1>
- agregada una pagina con 3 carouseles!

19/04/2011

- vivo, quite el titulo <header>
- se modificaron los html y css de compartir..... cuando se implemente los botones de FB y TW tener muy encuenta los parametros que
se están pasando.... en todo caso preguntar.
- se modificó el javascript de init.js para que no ejecute tanto codigo al cargar
- se agregaron eventos para inicializar pagina de nota-interna y home en tn.js.... TN.home_setup y TN.setup_nota_interna
- se agregaron las paginas de registro, login, reseteo de contraseña

20/04/2011

- se agregaron los popup de reportar abuso y enviar por email
- a los <a class="reportar"> se debe agregar data-id="id-comentario"
- a los <a class="icon-email"> se debe agregar data-id="id-nota"

21/04/2011

- en las paginas de login se puso el <h1> dentro del form


26/04/2011
- se generaron los js mimificados de produccion y se activaron
- se actualizo la versión de jquery
- se instalaron los scripts de eplaning
- se elimino la clase main-comunity
- se modifico el HTML del banner top998
- se modificio el widget de trafico, no se usa mas un <ul>, ahora es una serie de <div>
- se actualizaron las posiciones de los botones de FB y TW en el flujo de notas
- primera version de integración del player de fotos y videos

27/04/2011

- se quito el <li class="sep">
- agregada la clase page-404 para la pagina 404

28/04/2011

- en los loginbox se reemplazo la clase "google" por "tn", y el texto tambien

29/04/2011

- cambiado los h2 del flujo por h1
- cambiado el <ul> del submenu

2/5/2011
- actualización de la rutina de lazyload
- se actualizó el submenu (se pasaron a h2 los links)
- se actualizó el footer (se pasaron a h2 los links)
- se pasó a h1 los nombres de usuarios e los comentarios denota interna
- se ajustó el css para los cambios de h2
- se agrego un container a las notas del flujo
- todos los links de ranking pasaron a h2
- se agrego un <div class="container clearfix"> al flujo
- se modifico el footer, se quito el "ver mas" de adentro de los bloques y se lo paso afuera, se agregaron containers
- actualización de links del footer y del sidebar a h2
- verificar el funcionamiento del captcha cuando se suba a pre/produccion/etc

3/5/2011

- se cambio el html del widget de transito, se agrego <div class="alignright"> al contenido de cada item
- se cambio el html del widget de transito, se agrego la clase "clearfix" a cada item
- se actualizó el player de fotos y videos
- se actualizaron las rutinas de javascript
- se actualizaron los iconos del widget de rating
- a todos los videos (<a class="play ) agregar la clase "play-video"
- a todos las galerias (<a class="play ) agregar la clase "play-photo"
- se quito enlazes patrocinados de la nota interna
- se actualizó el html del logo del carrousel de TN y la Gente

4/5/2011

- se modifico el html y el js que lanza la busqueda de twitter
- se puso <h2> en los links de "ver todos"
- se unifico html para cuando no tienen sidebar, agregar la clase "no-sidebar" a estos casos.
- se modificaron los <li> dentro del ranking, favor de mirarlos, gracias.
- se modifico el html del sidebar de widget de 'periodistas en twitter'
- se modifico la parte de elimar avatar y cambiar avatar del formulario de comunidad.

5/5/2011

- se modifico el share de la nota, ahora son 2 <ul>
- se puso <h2> a los links de "lo ultimo"
- se actualizó el orden del sidebar de la home y nota interna
- se modifico el tag de figure de la nota interna
- se actualizo los textos de la reacciones de la nota interna
- se modifico el widget de transito de la home
- se actualizaron los botones de reacciones
- se implementarion las reacciones en nota interna
- se puso <h2> en "mas noticas" del sidebar
- se puso el player de video en "mas noticias" y en la grilla debajo del titular de la home
- se actualizo los botones de reaciones

6/5/2011

- se puso una caja de compartir en el titular del home
- se actualizó la caja de twitter del sidebar
- se cambiarion los botones de like por botones de recomendar en el flujo
- se agrego un boton de ver mas al widget de tw
- agregar al hgroup del header del home, la clase "clearfix"
- se modifico la encuesta, se puso una tabla

7/5/2011

- correccion a la carga de scripts en iframes y general
- se puso el acento a "tambien" en la nota interna
- comunidad, se puso los : que faltaban en los form
- se puso la clase "first" al primer comentario de la nota interna
- se puso la clase "first" al primer comentario de la pagina de comentarios
- se cambio un poco el html de la pagina de comentario, ahora esta mas igual al codigo de comentario normal
- js para el boton de ranking
- ajuste de css para twtter widgets
- ajuste de css para blogs y opinion
- ajuste de html y css en el footer
- ajuste de css para los alert de header

9/5/2011

- modifique el html del resultado de la encuesta de la nota interna, mirar detenidamente los <li>
- se agregó la bajada a la nota interna
- se quito el rollover de la imagen de nota interna
- se ajusto el css
- se ajusto las paginas de registro y de sincronizacion
- se puso la clase "forgot-pass" al form de olvido de contraseña
- los titulares de la pagina de secciones/tags son <h2>
- el <header> de la pagina de secciones/tags se puso dentro del section news
- se modifico la landing page de opinion
- se modifico el <header> de archivo; se agrego una clase "file-news" y se puso el tag <time>

10/5/2011

- actualizacion del código del player
- se modificaron las cajas de notas sin fotos
- se actualizó el marckup y el css de programas y ficha de programas

11/5/2011

- se modificó el js de carga inicial y el myhead
- se actualizó el css
- se agregó un sking de player para videos chicos.. y se ajustó el css y el markup que genera
- se agrego una llamada a un css para imprimir
- se actualizaron los textos de las reacciones
- se actualizaron los textos del sidebar
- se actualizaron los textos de comunidad
- se actualizaron los textos de comentarios
- se modificaron los login box, ahora va la imagen antes que el contenido
- se modifico la parte de transito de subtes, revisar bien

12/5/2011

- nota interna, comentarios; hay una clase middle, la cambie por replys
- comunidad, se agregaron clases a los formularios de registro, olvido de contraseña, y registro
- nota de opinion, a las notas que tienen avatar le agrege un <div class="content">
- en el recaptcha, agrege un div contenedor

16/5/2011

- cambie los titulos de los tags/secciones, ahora debe ser asi:
    <header class="flow">
      <ul class="subscribe">
        <li class="title">Suscribite</li>
        <li class="link first"><a class="rss" href="#">RSS</a></li>
      </ul>
      <h2>TAG/SECCION</h2>
    </header>

17/5/2011

 - se estandararizon los share; ahora las clases son "share share-SECCION'
 Shares del sitio
  - home: titular
  - flow: flujo; pagina de opinon
  - more: en mas comentadas/leidas/valoradas
  - nota: nota interna; nota de opinion; videos
  - comment: comentario de una nota
  - section: secciones/tags
  - programs: pagina de programas; ficha del programa

 - se agrego un link de reply en la alertas

02/06/2011

 Se agrego el logo de hd, hacer el siguiten cambio en el header grande y pequeño

         <div class="logo">
          <a id="tn-home" href="#">TN</a>
          <span title="El primer canal de noticias de Argentina en HD" class="hd">HD</span>
        </div>

06/06/2011

 se agrego <span class="bg"></span> en los siguientes lados

 Grid del site
 Imperdibles del sidebar del home
 Foto del home
 Relacionadas de la nota
 Listado de programas

 Por ahora esta comentado, descomentar linea 66 del css, y comentar 477

08/06/2011

- comunidad, registro; se cambio el texto del submit

14/06/2011

- Se agrego la pagina informativa del reseteo de contraseña
- se agrego esto <a name="reset-pass" ></a> a la pagina de los datos personales, va antes de "cambiar contraseña"
- se modifico el html mde fb:send, debe estar encerrado en un condicional de esta manera para la nota interna:

        <!--[if (gt IE 6)|!(IE)]><!-->
        <li class="fbsend">
          <fb:send href="http://dozer.pressbox.com.ar/output/nota-interna/nota-interna-tweet-ask.html" ref="send_button_ni"></fb:send>
        </li>
        <!--<![endif]-->

y de esta manera para el resto del sitio

            <!--[if (gt IE 6)|!(IE)]><!-->
            <div class="fbsend">
              <fb:send href="http://dozer.pressbox.com.ar" width="66px"></fb:send>
            </div>
            <!--<![endif]-->

 Noten que en la nota interna le quite el style y le agregue una clase


16/06/2011

- Agrege el link en el footer, es el ultimo de "secciones".
- Agregue un link mas en "programas", tambien es el ultimo.
- Esto hace que se pueda poner un link mas en temas y personajes.

29/06/2011

- se agrego un nuevo espacio de publicidad en el sidebar de todo el sitio
- se agrego el js en el footer para que se use el nuevo espacio de publicidad

07/07/2011

- se agrego un header-ticket para las eleeciones.
- se agrego una clase "bunker" al <li> para el item de la eleccion.
- se agrego la pagina /home/home-elecciones.html como demo.

10/8/2011
- se expandió el boton de share de FB a 141 pixeles

15/08/2011
- se agregó el link a Vivo HD en el vivo común

23/08/2011
- se actualizó la pagina de GPS, con textos e imagenes

04/10/11

- nota interna, votacion de "que sentis", se cambio el boton de votacion de <a> por <button>

05/10/11

- paginador; ahora todos los los paginadores, salvo el de la pagina de "todos los comentarios" van dentro de
<footer class="pager clearfix"></footer>

- caja de la grilla expandible del header, "imperdibles" del sidebar, grilla debajo de la nota principal, relacionadas de la nota interna, relacionadas de cine/teatro, y similar;  todas esas cajitas se cambio el markup para cuando tienen imagen, antes era

<figcaption>
<a href="#" class="url fn entry-title" rel="bookmark" target="">¿El final de Los Simpson?</a>
</figcaption>

y ahora es

<figcaption>
<a title="¿El final de Los Simpson?" href="#" rel="bookmark" class="url fn"><span class="entry-title">¿El final de Los Simpson?</span></a>
</figcaption>



- header; intercambie de lugar la clase "logo" con la clase "header-meta", antes era

<div class="logo">contenido</div>
<div class="header-meta">contenido</div>

ahora es

<div class="header-meta">contenido</div>
<div class="logo">contenido</div>


- los paginadores, ahora tienen rel="next" cuando es "ver mas" o siguiente, y rel="prev" cuando es anterior

- nota interna, publicador; el ul que tiene el texto "publicar en", ahora es <ul id="publish-to" class="clearfix">

- pagina del comentario; al <a> que esta dentro del h2, se le puso la clase "entry-title"


18/10/2011

- nota interna, usuario no logueado, en la caja de loguearse que esta debajo de los comentarios, al <ul> hay que agregarle la clase "not-logged-login"

25/10/2011

- comunidad, login form, se agrego luego del boton de submit un fieldset, agregarlo.
- nota interna, antes de <div class="review-note"> agrege <a name="reaction"></a>

26/10/2011

- luego del cierre del <div class="logo"> agregar

        <div id="headerad">
        	<span class="bg"></span>
        </div>

13/08/2013
Fixture
    Se cambio el banner en los html de los distintos torneos


    premierleague.html

        <div class="estadisticas-fixture">
            <img src="http://172.16.49.241/tn/site/output/images/banners/premierleague-v2.png" width="947" height="113">

    primera-a.html

        <div class="estadisticas-fixture">
            <img src="http://172.16.49.241/tn/site/output/images/banners/nacionala-v2.png" width="947" height="113">

    nacional-b.html

        <div class="estadisticas-fixture">
            <img src="http://172.16.49.241/tn/site/output/images/banners/nacionalb-v2.png" width="947" height="113">

    supercampeon.html

        <div class="estadisticas-fixture">
            <img src="http://172.16.49.241/tn/site/output/images/banners/nacionala-v2.png" width="947" height="113">

    calcioitaliano.html

        <div class="estadisticas-fixture">
            <img src="http://172.16.49.241/tn/site/output/images/banners/calcioitaliano-v2.png" width="947" height="113">

    copasudamericana.html

        <div class="estadisticas-fixture">
            <img src="http://172.16.49.241/tn/site/output/images/banners/copasudamericana-v2.png" width="947" height="113">

    ligaalemana.html

        <div class="estadisticas-fixture">
            <img src="http://172.16.49.241/tn/site/output/images/banners/ligaalemana-v2.png" width="947" height="113">

    ligadeespana.html

        <div class="estadisticas-fixture">
            <img src="http://172.16.49.241/tn/site/output/images/banners/ligadeespana-v2.png" width="947" height="113">

    brasileirao.html

        <div class="estadisticas-fixture">
            <img src="http://172.16.49.241/tn/site/output/images/banners/brasileirao-v2.png" width="947" height="113">

    eliminatoriasconmegol.html

            <div class="estadisticas-fixture">
                <img src="http://172.16.49.241/tn/site/output/images/banners/conmegol-v2.png" width="947" height="113">
