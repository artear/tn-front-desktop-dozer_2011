# encoding: UTF-8
module Texts

  extend self

  def ini()
    @titles = Array.new

    @titles << 'Ronaldo y Messi, la lucha por la hegemonia en el futbol'
    @titles << 'Schoklender no quiere que Oyarbide siga llevando la causa contra él'
    @titles << 'La “semi plena prueba” de Aníbal F.'
    @titles << 'Sale la "gran" Noelia, entra la "enana" Feudale'
    @titles << 'Peratta: "En Rosario hay mala leche y extorsionadores"'
    @titles << 'Cumbre por la crisis de San Lorenzo'
    @titles << 'Di Caprio, en su mejor estado'
    @titles << 'Un esguince no tan Chiquito'
    @titles << 'Reunión en Trabajo para destrabar el bloqueo en Ezeiza y Aeroparque'
    @titles << 'Santa Cruz: enfrentamientos entre facciones de la UOCRA'
  end

  def title
    @titles.shuffle.first
  end


end

include Texts
Texts.ini()
