def get_ads(section)

  ads = Array.new

  case section
    when 'homev2'
      ads << 'Top950x50'
      ads << 'Right1x300x250'
      ads << 'Right2x300x250'
      ads << 'Right3x300x250'
      ads << 'Right4x300x250'
      ads << 'megabanner'
      ads << 'Skyx120x600IZQ'
      ads << 'Skyx120x600DER'
 
    when 'especial'
      ads << 'Top950x50'
      ads << 'Skyx120x600IZQ'
      ads << 'Skyx120x600DER'
     
    when 'musica'
      ads << 'Top950x50'
      ads << 'Right1x300x250'
      ads << 'Right2x300x250'
      ads << 'Right3x300x250'
      ads << 'Right4x300x250'
      ads << 'megabanner'
      ads << 'Skyx120x600IZQ'
      ads << 'Skyx120x600DER'
      
    when 'show'
      ads << 'Top950x50'
      ads << 'Right1x300x250'
      ads << 'Right2x300x250'
      ads << 'Right3x300x250'
      ads << 'Right4x300x250'
      ads << 'megabanner'
      ads << 'Skyx120x600IZQ'
      ads << 'Skyx120x600DER'
      
    when 'deportes'
      ads << 'Top950x50'
      ads << 'Right1x300x250'
      ads << 'Right2x300x250'
      ads << 'Right3x300x250'
      ads << 'Right4x300x250'
      ads << 'megabanner'
      ads << 'Skyx120x600IZQ'
      ads << 'Skyx120x600DER'
      
    when 'tag'
      ads << 'Top950x50'
      ads << 'Right1x300x250'
      ads << 'Right2x300x250'
      ads << 'Right3x300x250'
      ads << 'megabanner'

    when 'reaccion'
      ads << 'Top950x50'
      ads << 'Right1x300x250'
      ads << 'Right2x300x250'
      ads << 'Right3x300x250'
      
    when 'personaje'
      ads << 'Top950x50'
      ads << 'Right1x300x250'
      ads << 'Right2x300x250'
      ads << 'Right3x300x250'
      ads << 'megabanner'  
      
    when 'nota'
      ads << 'Top950x50'
      ads << 'Right1x300x250'
      ads << 'Right2x300x250'
      ads << 'Right3x300x250'
      
    when 'politica'
      ads << 'Top950x50'
      ads << 'Right1x300x250'
      ads << 'Right2x300x250'
      ads << 'Right3x300x250'
      ads << 'megabanner'
      
    when 'programas'
      ads << 'Top950x50'

    when 'adosvoces'
      ads << 'Top950x50'
      ads << 'Right1x300x250'
      ads << 'Right2x300x250'
    
    when 'fixture'
      ads << 'Right1x300x250'
      ads << 'Skyx120x600IZQ'
      ads << 'Skyx120x600DER' 
       
  end
  
  return '"' + ads.join('","') + '"';

end
