$LAB.script(TN.BASE_PATH + 'js/libs/jquery-164_min.js').wait()
    .script(TN.BASE_PATH + 'js/prod/weather-min.js');

if (head.browser.ie)  {
	//load d_belatedpng to fix png32 in ie6
	if (parseInt(head.browser.version) <= 6)   {
		$LAB.script(TN.BASE_PATH + 'js/libs/dd_belatedpng.js',TN.BASE_PATH + 'js/libs/pngfix.js');
	}
}
