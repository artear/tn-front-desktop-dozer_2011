<!doctype html>
<!--[if lt IE 7 ]> <html class="no-js ie ie6"> <![endif]-->
<!--[if IE 7 ]> <html class="no-js ie ie7"> <![endif]-->
<!--[if IE 8 ]> <html class="no-js ie ie8"> <![endif]-->
<!--[if IE 9 ]> <html class="no-js ie ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="es" class="no-js"> <!--<![endif]-->
  <head>
        <meta charset="utf-8" />

    <title></title>

    <meta property="og:image" content="http://dozer.pressbox.com.ar/output/images/tn-logo2.png" />
    <meta property="og:site_name" content="Todo Notcias" />
    <meta property="og:region" content="Buenos Aires" />
    <meta property="og:country-name" content="Argentina"/>
    <meta property="fb:app_id" content="197081563653884" />

    <meta name="description" content="" />
    <meta name="author" content="" />

    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="apple-touch-icon" href="/apple-touch-icon.png" />

    
      <link rel="stylesheet" href="css/reset.css" type="text/css" />
      <link rel="stylesheet" href="css/iframe-fixture.css" type="text/css" />
    

    
    <!--[if IE ]>
      <link rel="stylesheet" href="css/iframe-fixture-ie.css" type="text/css" />
    <![endif]-->    
    
  </head>
  <body id="widget-fixture" class="widget">
  
    <div class="header">
            <div class="title">
        <h2>Torneo Primera División</h2>
        <form id="otros-torneos">
          <select>
            <option>Selecciona la Categoría:</option>
          </select>
        </form>
      </div>
      <ul>
        <li ><a href="fixture.html">Fixture</a></li>     
        <li ><a href="posiciones.html">Posiciones</a></li>
        <li ><a href="goleadores.html">Goleadores</a></li>
        <li ><a href="descenso.html">Descenso</a></li>
      </ul>
    </div>
    
    for(var i=0; i<document.images.length; i++) {
	var img = document.images[i];
	var imgName = img.src.toUpperCase();
	if (imgName.substring(imgName.length-3, imgName.length) == "PNG"){
		var imgID = (img.id) ? "id='" + img.id + "' " : "";
		var imgClass = (img.className) ? "class='" + img.className + "' " : "";
		var imgTitle = (img.title) ? "title='" + img.title + "' " : "title='" + img.alt + "' ";
		var imgStyle = "display:inline-block;" + img.style.cssText;

		if (img.align == "left"){
			imgStyle = "float:left;" + imgStyle;
		}
		if (img.align == "right"){
			imgStyle = "float:right;" + imgStyle;
		}

		var strNewHTML = "<span " + imgID + imgClass + imgTitle;
		strNewHTML += " style=\"" + "width:" + img.width + "px; height:" + img.height + "px;" + imgStyle + ";";
		strNewHTML += "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader";
		strNewHTML += "(src=\'" + img.src + "\', sizingMethod='scale');\"></span>"; 
		img.outerHTML = strNewHTML;

		i = i-1;
	}
}
    
    
  
  </body>
</html>