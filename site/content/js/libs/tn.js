TN.warming_up = false;
TN.d = undefined;
TN.r = undefined;

TN.adsetup = function(){
    eplArgs = { iIF:1,
		        sV:"http://ads.e-planning.net/",
		        vV:"4",
		        sI:"14f7b",
		        sec:TN.ads.sec,
		        eIs:TN.ads.eIs
     }
	if (TN.ads.ss != undefined){
		eplArgs.ss = TN.ads.ss;
	}
    
	TN.ads.jqe = TN.eplanning( {
				        eplDoc: document,
				         eplLL: false,
				           eS1: 'us.img.e-planning.net',
				       eplArgs: eplArgs
				          });	
	
	var l = TN.ads.eIs.length;
	for (i = 0; i < l; i++){
		//var container = '#';
		/*
		if (isNaN(i)) {
			container = '#' + key;
		} else {
			container += TN.ads.eIs[i];
		}
		*/
		var container = '#' + TN.ads.eIs[i];

		if ($(container).length > 0){ 
			TN.ads.jqe.add(container, TN.ads.eIs[i]);
		}
		
	}

}

    // http://jdbartlett.github.com/innershiv | WTFPL License
TN.innershiv = function(h,u) {
  if (TN.d === undefined) {
      TN.d = document.createElement('div');
      TN.r = document.createDocumentFragment();
      /*@cc_on TN.d.style.display = 'none';@*/
  }
  
  var e = TN.d.cloneNode(true);
  /*@cc_on document.body.appendChild(e);@*/
  e.innerHTML = h.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
  /*@cc_on document.body.removeChild(e);@*/

  if (u === false) return e.childNodes;

  var f = TN.r.cloneNode(true), i = e.childNodes.length;
  while (i--) f.appendChild(e.firstChild);

  return f;
};

// rutina usada para la carga asyncronica de los botones de facebook y twitter
TN.load_social_iframe = function() {
  if (!this.loaded) {
    var $t = $(this);
    this.contentWindow.location.replace($t.attr('data-src'));
    this.loaded = true;
  }
};

// rutina usada para la carga asyncronica de los botones de twitter
TN.load_twitter_button = function() {
  if (this.loaded) { return; }
  var $t = $(this);
  $t.append( $('<iframe allowtransparency="true" frameborder="0" scrolling="no" width="'+$t.data('width')+'" height="'+$t.data('height')+'" src="'+$t.data('src')+'"></iframe>') );
  this.loaded = true;
}

TN.fb_get_html = function(fb_params){

	  var acepted = new Array('like');
	  var replace = false;

	  var type = fb_params.type.toLowerCase();
	  delete fb_params.type;

	  for (i in acepted) {
		  if (acepted[i] == type) {
			  replace = true;
			  break;
			}
	  }
	  if (replace == false) {return false}

	  var tmp = new Array();
	  var i = 0;

	  for (e in fb_params) {
		  tmp[i] = e + '="' + fb_params[e] + '"';
		  i++;
		}

	  return '<fb:' + type + ' ' + tmp.join(' ') + '></fb:' + type + '>';

}

TN.load_fb_asyn = function() {
	var $fb = $(this);
	if ($fb.data('loaded') == false){
		fb_params = $fb.data('fbparams')//$.parseJSON($fb.data('fbparams'));

		$fb.html(TN.fb_get_html(fb_params));
		if (typeof(FB) != 'undefined' && FB != null ) {
			FB.XFBML.parse($fb[0]);
		}
		$fb.data('loaded', 'true')
	}
};

// rutina usada para la carga asyncronica de los botones de google
TN.load_glg_button = function() {
  if (window.gapi === undefined) { return false; }
  if (!this.loaded) {
    var $t = $(this);
    this.loaded = true;
    gapi.plusone.render(this, {'size': 'medium', 'count': 'true', 'href': $t.data('href') });
  }
};

// Configura el AppID de TN
TN.setup_tn_facebook = function() {
  window.fbAsyncInit = function() {
    // TN PROD
    FB.init({appId: '346699902992', status: true, cookie: true, xfbml: true});
    // TN PRE
    //FB.init({appId: '216375521778597', channelUrl: '//tn.com.ar/channel.html', status: true, cookie: true, xfbml: true});
    /*
    if ( ($('body').hasClass('not-logged-in') == true) && ($.cookie('fbautologin') == null) ){
      var fbid = FB._userID;
      // Remove for prod
      //fbid = 234324;
      if (fbid != 0) {
      	if ($('#new').length > 0) {
      		fbid = fbid + '?path=node/' + $('#new').data('node');	
      	}
        var url = '/user/fb_autologin/' + fbid;
        // Remove for prod
        //var url = '/api/fb';
        var jqxhr = $.ajax({type: "POST",
                            url: url,
                            dataType: "json",
                            async: true,
                            cache: false
                          });
    
        jqxhr.success(function(data) {
          if (data == null){
        	  return false;
          }
          $('#header-top .login-box').remove();
          $('#header-top .wrapper').append(TN.innershiv(data.loginbox, false));
          
          if ($('#new').length > 0) {
        	  var $comments = $('section.comments');
        	  $comments.height($comments.height());
        	  $('section.comments footer').remove();
        	  $comments.append(TN.innershiv(data.comments,false));
        	  $comments.height('auto');
          }
          
        });
        //jqxhr.error(function(jqXHR, textStatus, errorThrown) {console.log(textStatus + ': ' + errorThrown);});        
      }
    }
    */
  };
};

// Carga la libreria de FB
TN.load_facebook_js = function() {
  var e = document.createElement('script');
  e.id = 'facebook-jssdk';
  e.async = true;
  e.src = '//connect.facebook.net/es_LA/all.js';
  document.getElementById('fb-root').appendChild(e);
};

TN.toggle_spinner_on_button = function($ele, $button) {
  if ($ele.is(':visible')) {
    $button.val($button.data('original_text'));
    $ele.hide();
  } else {
    $button.data('original_text', $button.val()).val('');
    $ele.show();
  }
};

TN.toggle_spinner_voto_nota = function(pos, e) {
  var $sp = $('#voto-spinner');
  if ($sp.is(':visible')) {
    e.html(e.data('original_text'));
    $sp.removeClass().hide();
  } else {
    e.data('original_text', e.html()).html('');
    $sp.removeClass().addClass('voto-spinner-' + pos).show();
  }
};

TN.tw_list = function(oid, user, list, width, height) {
  var twlist = new TWTR.Widget({
    'id': oid,
    'creator': 'tn',
    'version': 2,
    'type': 'list',
    'rpp': 30,
    'interval': 6000,
    'title': '',
    'subject': '',
    'width': width,
    'height': height,
    'theme': { 'shell': { 'background': '#fff', 'color': '#444' }, 'tweets': { 'background': '#ffffff', 'color': '#444444', 'links': '#0056C3' } },
    'features': { 'scrollbar': true, 'loop': true, 'live': true, 'hashtags': true, 'timestamp': true, 'avatars': true, 'behavior': 'all' }
  });
  twlist.render().setList(user, list);
  return twlist;
};

TN.tw_search = function(oid, width, height, search ) {
  var twlist = new TWTR.Widget({
    'id': oid,
    'creator': 'tn',
    'version': 2,
    'type': 'search',
    'search': search,
    'interval': 5000,
    'title': '',
    'subject': '',
    'width': width,
    'height': height,
    'theme': { 'shell': { 'background': '#fff', 'color': '#444' }, 'tweets': { 'background': '#ffffff', 'color': '#444444', 'links': '#0056C3' } },
    'features': { 'scrollbar': true, 'loop': true, 'live': true, 'hashtags': true, 'timestamp': true, 'avatars': true, 'toptweets': true, 'behavior': 'default' }
  });
  twlist.render();
  return twlist;
};

TN.reactions = function() {
  var that = {};
  that.rpp = 10;
  that.fsel = true;

  var html = '<article class="clearfix %first%"> \
                <a href="http://twitter.com/%ulink%" target="_blank" class="avatar"><img width="48" height="48" class="lzl" src="%uimage%" style="display: inline;"></a> \
                <div class="comment-body"> \
                  <div class="meta"> \
                    <h1><a href="http://twitter.com/%ulink%" target="_blank" title="usuario" class="user-profile">%uname%</a></h1> \
                  </div> \
                  <div class="content">%text%</div> \
                  <ul class="respond"> \
                    <li class="first"><time>%time%</time></li> \
                    <li><a href="http://twitter.com/intent/tweet?in_reply_to=%reply%&hashtags=TodoNoticias&related=todonoticias" target="_blank">Responder</a></li> \
                  </ul> \
                </div> \
              </article>';


  /**
    * relative time calculator
    * @param {string} twitter date string returned from Twitter API
    * @return {string} relative time like "2 minutes ago"
    */
  var timeAgo = function(dateString) {
    var rightNow = new Date();
    var then = new Date(dateString);

    if (head.ie) {
      // IE can't parse these crazy Ruby dates
      then = Date.parse(dateString.replace(/( \+)/, ' UTC$1'));
    }

    var diff = rightNow - then;

    var second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24,
        week = day * 7;

    if (isNaN(diff) || diff < 0) { return ""; } // return blank string if unknown
    if (diff < second * 2) { return "recién"; } // within 2 seconds
    if (diff < minute) { return "hace cerca de " + Math.floor(diff / second) + " segundos"; }
    if (diff < minute * 2) { return "hace cerca de 1 minuto"; }
    if (diff < hour) { return "hace cerca de " + Math.floor(diff / minute) + " minutos"; }
    if (diff < hour * 2) { return "hace cerca de 1 hora"; }
    if (diff < day) { return  "hace cerca de " + Math.floor(diff / hour) + " horas"; }
    if (diff > day && diff < day * 2) { return "ayer"; }
    if (diff < day * 365) { return "hace cerca de " + Math.floor(diff / day) + " días"; }
    else { return "hace más de un año"; }
  };

  /**
    * The Twitalinkahashifyer!
    * http://www.dustindiaz.com/basement/ify.html
    * Eg:
    * ify.clean('your tweet text');
    */
  var ify = {
    link: function(tweet) {
      return tweet.replace(/\b(((https*\:\/\/)|www\.)[^\"\']+?)(([!?,.\)]+)?(\s|$))/g, function(link, m1, m2, m3, m4) {
        var http = m2.match(/w/) ? 'http://' : '';
        return '<a class="twtr-hyperlink" target="_blank" href="' + http + m1 + '">' + ((m1.length > 25) ? m1.substr(0, 24) + '...' : m1) + '</a>' + m4;
      });
    },

    at: function(tweet) {
      return tweet.replace(/\B[@＠]([a-zA-Z0-9_]{1,20})/g, function(m, username) {
        return '@<a target="_blank" class="twtr-atreply" href="http://twitter.com/' + username + '">' + username + '</a>';
      });
    },

    list: function(tweet) {
      return tweet.replace(/\B[@＠]([a-zA-Z0-9_]{1,20}\/\w+)/g, function(m, userlist) {
        return '@<a target="_blank" class="twtr-atreply" href="http://twitter.com/' + userlist + '">' + userlist + '</a>';
      });
    },

    hash: function(tweet) {
      return tweet.replace(/(^|\s+)#(\w+)/gi, function(m, before, hash) {
        return before + '<a target="_blank" class="twtr-hashtag" href="http://twitter.com/search?q=%23' + hash + '">#' + hash + '</a>';
      });
    },

    clean: function(tweet) {
      return this.hash(this.at(this.list(this.link(tweet))));
    }
  };

  var create_tweet = function(twt) {
    var h = html.replace(/%uimage%/g, twt.profile_image_url)
                .replace(/%uname%/g, twt.from_user)
                .replace(/%text%/g, ify.clean(twt.text))
                .replace(/%ulink%/g, twt.from_user)
                .replace(/%time%/g, timeAgo(twt.created_at))
                .replace(/%reply%/g, twt.id_str);
    if ((that.frst) && (that.fsel)) {
      h = h.replace('%first%', 'first');
      that.fsel = false;
    } else {
      h = h.replace('%first%', '');
    }
    return h;
  };

  var on_success = function( data, status, xhr) {
    var $r = $('#reactions');
    var $mr = $('#more-reactions');
    $.each(data.results, function(i,e) {
      $r.append(TN.innershiv(create_tweet(e)));
    });
    if ((data.results.length === 0 ) && (that.frst)) {
      $('#reactions-wrap').hide();
    }
    if (data.results.length === that.rpp) {
      $mr.attr('rel', parseInt($mr.attr('rel'), 10)+1).show();
    } else {
      $mr.hide();
    }
  };

  var on_error = function( xhr, status, error) {};

  var getData = function(frst) {
    that.frst = frst;
    var page = $('#more-reactions').attr('rel');
    var url = window.location.href;
    $.ajax({'url': 'http://search.twitter.com/search.json',
            'async': true,
            'dataType': 'jsonp',
            'type': 'GET',
            'data': {'q': url, 'page': page, 'rpp': that.rpp, 'result_type': 'mixed'},
            'success': on_success,
            'error': on_error
           });
  };
  that.getData = getData;

  return that;
};

// inicializa js para página de Vivo
TN.setup_vivo = function() {
	TN.setup_tn_facebook();
	TN.load_facebook_js();
  //cplayer('div.cplayer');
	$('.dplayer').dplayer();
  setInterval( 'TN.show_admovil()', 5000 );
};

TN.show_admovil = function() {
  $('#admovil a').fadeIn('slow');
}

// inicializa js para programa
TN.setup_programas = function() {
	TN.setup_tn_facebook();
	TN.load_facebook_js();
  //cplayer('a.cplayer');
	$('.dplayer').dplayer({skin:'/sites/all/themes/dozer/swf/dplayer/tnplayer230.zip'});
};

TN.setup_publish = function(){
	TN.init_sync_accounts_tooltip();
	TN.toggle();
	TN.nopublish();
};

TN.nopublish = function(){
	$('form').submit(function(){
		if ($(this).data('submit') == 'true') {
			return false;
		}

		$(this).data('submit', 'true');
		$(this).find('input[type="submit"]').addClass('disabled')
	});
}

TN.toggle = function(){
  $('#content .toggle label, #content .toggle .arrow').click(function(){
    var $container = $(this).parents('.toggle');
    
    if ($container.filter(":animated").length == 1){
      return false;
    }
    
    var $content = $container.find('.content');
    
    $content.css('display', 'block');
    var h = $content.height();
    
    if ( $container.hasClass('colapse') ) {
      $container.animate({height: '+='+h},1000, function(){
                                                  $container.removeClass('colapse');
                                                  $container.css('height', 'auto');
                                                  $container.find('.arrow').removeClass('down').addClass('up');
                                                });  
    }else{
      $container.animate({height: '35'},500, 
        function(){
          $container.addClass('colapse');
          $container.find('.arrow').removeClass('up').addClass('down')
          $container.find('.content').css('display', 'none');
         });
    }  
    
  });
}

TN.init_sync_accounts_tooltip = function() {

	  if ($('#publish-to li.facebook').hasClass('disabled')) {
			$('#publish-to li.facebook').tooltip({
			    effect: 'slide',
			    relative: 'true',
			    tip: '#tooltip-facebook',
			    delay:'3000',
			    events: {def: 'click, mouseleave'}
			}).dynamic({ bottom: { direction: 'down', bounce: true } });

		  $('#publish-to li.facebook').click(function (e){
			  e.stopPropagation();
			  return false;
		  });
	  }

	  if ($('#publish-to li.twitter').hasClass('disabled')) {
			$('#publish-to li.twitter').tooltip({
			    effect: 'slide',
			    relative: 'true',
			    tip: '#tooltip-twitter',
			    delay:'3000',
			    events: {def: 'click, mouseleave'}
			}).dynamic({ bottom: { direction: 'down', bounce: true } });

		  $('#publish-to li.twitter').click(function (e){
			  e.stopPropagation();
			  return false;
		  });
	  }

}

TN.validate_textarea = function() {
	  $('.comments form.logged').submit(function(){
		  var $textarea = $(this).find('textarea');
		  if( $textarea.val() == '' ) {
			  alert('No se puede enviar un comentarios vacio');
			  return false;
		  }
	  });

	  if ($('#edit-comment').length > 0) {
		  $('#edit-comment').keycounter({'max':300, 'button': '#edit-form .submit'});
		  $('#edit-comment').trigger('keyup');
	  }
}

TN.setup_comentario_nota = function() {
  TN.init_sync_accounts_tooltip();
	TN.validate_textarea();
};

TN.setup_cartelera = function () {

	TN.init_codas();

  $('#releases .arrows').css( {height: $('#releases .content-wrapper').height()}  );

  $('#releases .arrows').removeClass('hidden');

  $('#releases .content').jCarouselLite({
      btnNext: '#releases .next',
      btnPrev: '#releases .prev',
      visible: 3,
      circular: false,
      speed: 1000
  });

  TN.react = TN.reactions();
  TN.react.getData(true);
  $('#more-reactions').click(function(ev) {
    ev.stopPropagation();
    TN.react.getData(false);
    return false;
  });

  TN.init_twitter_widgets();

  $('#overlay form').live('submit', function() {
	  var $overlay = $('#overlay');
	  $overlay.find('.content').append('<div class="loading"></div>');
	  $(this).find('.submit').attr('disabled', 'disabled');

	  var jqxhr = $.ajax({type: "POST",
					url: $(this).attr('action'),
					data: $(this).serialize(),
					dataType: "html",
					async: false,
					cache: false
				});

		jqxhr.success(function() {
			$overlay.find('.loading').remove();
			$overlay.find('.submit').removeAttr('disabled');
     		$overlay.find('div.active').removeClass('active');
	        $overlay.find('div.panel-success').addClass('active');
		      $('#overlay div.content').animate({height: $('#overlay div.active').height() }, 300);
		      $('#overlay').animate({marginTop: ($('#overlay div.active').height() + $('#overlay div.header').height() )/-2},300);
		});

		jqxhr.error(function() {
			$overlay.find('.loading').remove();
			$overlay.find('.submit').removeAttr('disabled');
		      $overlay.find('div.active').removeClass('active');
		      $overlay.find('div.panel-error').addClass('active');
		      $('#overlay div.content').animate({height: $('#overlay div.active').height() }, 300);
		      $('#overlay').animate({marginTop: ($('#overlay div.active').height() + $('#overlay div.header').height() )/-2},300);
		});

	  return false;
  });

};

// inicializa js para nota-interna
TN.setup_nota_interna = function() {
  cplayer('a.cplayer');
  $('a.dplayer').dplayer();
  TN.init_notas_icons();
  TN.init_share_vote();
  window.setTimeout( TN.init_delayed_nota_interna, 2000 );
}; // end setup_nota_interna


TN.init_delayed_nota_interna = function() {
  TN.init_codas();
  TN.init_twitter_widgets();
  TN.init_reacciones();
  TN.init_sync_accounts_tooltip();
  TN.init_report_form();
  TN.init_voto_reacciones();
  TN.validate_textarea();
  // Inicializa los flyouts en las notas internas
  TN.init_flyout();
  TN.init_encuesta();
  //TN.thermometer();
  //jqe.showChapa('.flujo-auspicio','Nota', 'Auspicio');
  iepinned();
  TN.realtime.init();
};

TN.report_abuse = function(){

  $('#new .icon-report').click(function(){
    if ($(this).hasClass('disabled')){
      return false;
    }
    
    showoverlay(this, 'article', '.report-note');
    
    url = $(this).attr('href');

    //alert(url);

    var jqxhr = $.ajax({type: "POST",
                          url: url,
                          dataType: "html",
                          async: true,
                          cache: false
                        });
    var $overlay = $('#overlay');
    
    jqxhr.success(function() {
        $overlay.find('div.active').removeClass('active');
        $overlay.find('div.panel-success').addClass('active');
        $('#overlay div.content').animate({height: $('#overlay div.active').height() }, 300);
        $('#overlay').animate({marginTop: ($('#overlay div.active').height() + $('#overlay div.header').height() )/-2},300);
    });

    jqxhr.error(function() {
          $overlay.find('div.active').removeClass('active');
          $overlay.find('div.panel-error').addClass('active');
          $('#overlay div.content').animate({height: $('#overlay div.active').height() }, 300);
          $('#overlay').animate({marginTop: ($('#overlay div.active').height() + $('#overlay div.header').height() )/-2},300);
    });
    
    return false;
    
  });
}

TN.init_notas_icons = function() {

  $('a.reportar').click(function (){
	  var parent = 'section';
	  showoverlay(this, parent);
	  return false;
  });

  $('#new a.icon-email').click(function (){
	  var parent = 'article';
	  showoverlay(this, parent, '.send-email');
	  return false;
  });

  $('#new a.icon-print').click(function () {
	  window.print();
	  return false;
  });
  
  TN.report_abuse();
}

TN.init_reacciones = function() {

  TN.react = TN.reactions();
  TN.react.getData(true);
  $('#more-reactions').click(function(ev) {
    ev.stopPropagation();
    TN.react.getData(false);
    return false;
  });

}

TN.init_share_vote = function() {

  $('#votes-share-fb').click(function(ev) {
    if ($(ev.target).is(':checked')) {
      $.cookie('tn_share_vote_fb', null, {'path': '/'});
    } else {
      $.cookie('tn_share_vote_fb', 'noshare', {'expires': 1, 'path': '/', 'secure': false});
    }
      $.cookie('tn_share_vote_fb');
  });

  $.cookie('tn_share_vote_fb');
  // Muestra activo o no el botón de compartir las reacciones en FB
  if ($.cookie('tn_share_vote_fb') === null) {
    $('#votes-share-fb input').attr('checked', true);
  } else {
    $('#votes-share-fb input').attr('checked', false);
  }

}

TN.init_report_form = function() {
  $('#overlay form').live('submit', function() {
	  var $overlay = $('#overlay');
	  $overlay.find('.content').append('<div class="loading"></div>');
	  $(this).find('.submit').attr('disabled', 'disabled');

	  var jqxhr = $.ajax({type: "POST",
					url: $(this).attr('action'),
					data: $(this).serialize(),
					dataType: "html",
					async: false,
					cache: false
				});

		jqxhr.success(function() {
			$overlay.find('.loading').remove();
			$overlay.find('.submit').removeAttr('disabled');
     		$overlay.find('div.active').removeClass('active');
	        $overlay.find('div.panel-success').addClass('active');
		      $('#overlay div.content').animate({height: $('#overlay div.active').height() }, 300);
		      $('#overlay').animate({marginTop: ($('#overlay div.active').height() + $('#overlay div.header').height() )/-2},300);
		});

		jqxhr.error(function() {
			$overlay.find('.loading').remove();
			$overlay.find('.submit').removeAttr('disabled');
		      $overlay.find('div.active').removeClass('active');
		      $overlay.find('div.panel-error').addClass('active');
		      $('#overlay div.content').animate({height: $('#overlay div.active').height() }, 300);
		      $('#overlay').animate({marginTop: ($('#overlay div.active').height() + $('#overlay div.header').height() )/-2},300);
		});

	  return false;
  });
};

TN.init_voto_reacciones = function() {

  $('#votes .button').live('click',function(ev) {
    ev.stopPropagation();

    if ($('body').hasClass('not-logged-in'))
    {
    	showoverlay(this, '.review-note');
    	var h = $(this).attr('href');
    	$('#overlay .link a:eq(0)').attr('href', h + '/facebook');
    	$('#overlay .link a:eq(1)').attr('href', h + '/twitter');
    	$('#overlay .link a:eq(2)').attr('href', h + '/tn');
    	return false
    }

    var $e = $(ev.target);
    var $r = $e.attr('data-rating');
    var $u = $e.attr('href');

    var $nt = $('#new .entry-title').html();
    var $nh = $('#new .entry-title').attr('href');
    var $nb = $('#new .drop-line').html();

    if ($('#new .photo').length != 0){
      var $media = new Object();
      $media.type = 'image';
      $media.src = $('#new .photo').attr('src');
      $media.href = $nh;
    }
    $.ajax($u,{
      async: false, cache: false, dataType: 'html',
      beforeSend: function(a, b) { TN.toggle_spinner_voto_nota($r, $e); },
      complete: function(a, b) { TN.toggle_spinner_voto_nota($r, $e); },
      success: function(data, status, xhr) {
        $('#votes').empty().html(data);
        TN.toggle_spinner_voto_nota($r, $e);
        if ($.cookie('tn_share_vote_fb') === null) {
          TN.FB({ message: $e.attr('data-txt'),
              attachment:{
              name: $nt,
              description: $nb,
              href: $nh,
              media: ($media)?[$media]:null
              }
            }).publish();
        };
      }});
    return false;
  });

}


TN.init_flyout = function() {

  var $flow = $('#flow-popup');

  if ($flow.length > 0) {
    var is_show = false;

    var $myflow = $('<div class="body-flow-popup"></div>');
    $myflow.html(TN.innershiv($flow.html())).appendTo('body');

    $(window).bind('scroll', function(){
      //If is ie6, exit
      if ($.browser.msie) {
        if (parseInt($.browser.version)== 6) {  return false; }
      }

      var top = $(window).scrollTop(); //me consigue el top
      var bottom = $(window).height() + top;

      if ($flow.offset().top <= top || $flow.offset().top <= bottom) {
        if (is_show == false){
          is_show = true;
          $myflow.animate({'right': '0'}, 'normal', 'swing');
        }

      } else {
        if (is_show == true){
          is_show = false;
          $myflow.animate({'right': '-' + $myflow.outerWidth(true) }, 'normal', 'swing');
        }
      }

    });

    $('.body-flow-popup .close').live('click', function(){
      $myflow.animate({'right': '-' + $myflow.outerWidth(true) }, 'normal', 'swing', function(){
        $myflow.remove();
        $flow.remove();
      });
      return false;
    });

    setTimeout('$(window).scroll()', 250);
  }

}

TN.init_encuesta = function() {

  if ($('#container .poll-widget').length == 0) { return; }

  $('.poll-widget .form-radio tr').click(function() {
        var $b = $(this).find('span');
        if ($b.hasClass('checked')) {
          return false;
        }
        var $input = $(this).find('input');

        $input.parents('.content').find('span').removeClass('checked');
        $input.parents('.content').find('input').removeAttr('checked');

        $b.addClass('checked');
        $input.attr('checked','checked');

        return false;
  });

  var cook = JSON.parse($.cookie('tn_voto_nota'));

  $('#container .poll-widget').each(function(){
    var $poll = $(this);

    if ($poll.data('poll') == 'closed') {
    	return false;
    }
    
    if ( (cook == null) || (cook[$poll.data('poll')] == undefined) ){
      //show form
      $poll.find('.result-container').remove();
      $poll.find('.content').animate({height: $poll.find('.form-container').height()},1000);


      $poll.find('form').submit(function(){
        if ($(this).find(':checked').length != 1 ){
          return false;
        }

        var $content = $(this).closest('.content');
        $poll.addClass('poll-result');

        $(this).append('<div class="loading"></div>');
        $(this).find('.submit').val('');
        $(this).find('.submit').attr('disabled', 'disabled');

        var jqxhr = $.ajax({type: "POST",
              url: $(this).attr('action'),
              data: $(this).serialize(),
              dataType: "html",
              async: false,
              cache: false
            });

        jqxhr.success(function(data) {
          $content.html(data);
          $poll.find('.content').animate(
            { height: $poll.find('.result-container').height()+13 },
            500,
            function(){
              $(this).css('overflow', 'visible');
            }
          );

          $content.find('.twitter-share-button').lazyload({appear: TN.load_twitter_button});
          $content.find('.fb-button').lazyload({appear: TN.load_fb_asyn});

        });

        jqxhr.error(function(xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        });

        return false;
      });

    } else {
      //show result
      $poll.find('.form-container').remove();
      $poll.find('.content').animate({height: $poll.find('.result-container').height()+19}, 1000, function(){
       $(this).css('overflow', 'visible');
      });
    }
    $poll.find('.loading').fadeOut(1000, function(){
      $(this).remove();
    });
  });

} // end init_encuesta

TN.setup_home = function() {

  cplayer('a.cplayer');
  $('a.dplayer').dplayer();

  TN.init_carousel_photos();

  TN.init_encuesta();

  window.setTimeout('TN.init_delayed_home()', 2000 );

}; // end setup_home

TN.init_delayed_home = function() {
  TN.init_history();
  TN.init_codas();
  TN.init_twitter_widgets();
  TN.init_buscador_peliculas();
  TN.init_carousel_twitter();
  //jqe.showChapa('.flujo-auspicio', 'HomeV2', 'Auspicio');
  TN.fix_scrapper();
  TN.heightfixture();
  TN.widget_youtube();
  iepinned();
  TN.realtime.init();
}

TN.widget_youtube = function(){
	if ($('#yt-viewed').length > 0 ) {

	  var yt_viewed = 'https://gdata.youtube.com/feeds/api/standardfeeds/AR/most_viewed?v=2&region=AR&alt=json&safeSearch=moderate&time=today&max-results=3';
	  TN.trends_ajax_calls( yt_viewed );

	}	
}

TN.heightfixture = function() {
	if ($('#ifixture').length > 0 ) {
		window.setTimeout( TN.fixfixture, 500 );
	}
}

TN.fixfixture = function(){
	if ($('#coda-slider-fixture', frames['ifixture'].document).length > 0 ){
		max = 0;

		$('#coda-slider-fixture .panel', frames['ifixture'].document).each(function(index) {
		    if ($(this).height() > max){
		    	max = $(this).height();
		    }
		});

		max = max + 56;
		$('#ifixture').animate({height:max},1000,'linear');
		return true
	}
	window.setTimeout( TN.fixfixture, 500 );
}

TN.fix_scrapper = function(){
	h = $('#content-top figure');
  if (h.length == 0) {return;}
  h = h.offset().top - $('#container').offset().top;
	$('#container .scrapper').css({'top': h})
}

TN.init_history = function() {

  // Hook into State Changes
  $(window).bind('statechange', function() {
    var st = window.History.getState();
    var $e = $('ul.all-news a[data-src="' + st.data.link + '"]');
   
    $.ajax('/flujo_ajax/'+st.data.oid,
           {async: true, cache: true, data: {section: st.data.link}, dataType: 'html',
            beforeSend: function(a, b) { $('ul.all-news .spinner').show(); },
            complete: function(a, b) { $('ul.all-news .spinner').hide(); },
            success: function(data, status, xhr) {
              $('div.main').empty().html(TN.innershiv(data,false));
              $('.main-stream img').lazyload();
              $('.main-stream div.twitter-share-button').lazyload({appear: TN.load_twitter_button});
              $('.main-stream .fb-button').lazyload({appear: TN.load_fb_asyn});
              //$('.main-stream div.g-plusone').lazyload({appear: TN.load_glg_button});
              cplayer('.main-stream a.cplayer');
              $('.main-stream a.dplayer').dplayer();
              //jqe.showChapa('.main-stream .flujo-auspicio', 'HomeV2', 'Auspicio');
              //$('body').trigger('scroll');
            }
            });
  });

  $('#all-news a').click(function(ev) {
    var $e = $(ev.target);
    if ($e.attr('data-src') == undefined) { return true; }
    ev.stopPropagation();
    $e.parents('#all-news').find('a.active').removeClass('active');
    $e.addClass('active');
    var lnk = $e.attr('data-src');
    History.pushState({'link': lnk, 'oid': $e.attr('data-id')}, $e.attr('data-title') + ' | TN.com.ar | Todo Noticias', '/' + lnk);
    return false;
  });
}

TN.init_buscador_peliculas = function() {

  $('#form_buscador_peliculas').submit(function(){
    var CARID = $('#select-movie-id').val();
    var CINEID= $('#select-cine-id').val();

    if(CARID!="todas"){
      var anchor = "";
      var CineText = $("#select-cine-id option:selected").text();
      if(CINEID!="todos")anchor="#"+encodeURI(CineText);
      window.location.href=window.location.origin+CARID+anchor;
      return false;
    }
    window.location.href = window.location.origin +"/cine/busqueda/"+CINEID;
    return false;
  });
}

TN.init_twitter_widgets = function() {
  // Inicializa el twitter de periodistas
  if ($('#twpress').length > 0) {
    TN.tn_twp = TN.tw_list('twpress', 'todonoticias', 'tn-com-ar', 310, 500);
    setTimeout('TN.tn_twp.start()', 3000);
  }
  
  if ($('#twpresstnyg').length > 0) {
	  
	if ($('body').hasClass('home') == true){
		w = 470;
	} else {
		w = 310
	}
    TN.tn_twp = TN.tw_list('twpresstnyg', 'todonoticias', 'tn-com-ar', w, 250);
    setTimeout('TN.tn_twp.start()', 3000);
  }
  

  // Inicializa el twitter de menciones de TN.com.ar
  if (($('#twmenciones').length > 0)) {
    TN.tn_src = TN.tw_search('twmenciones', 310, 500, $('#twmenciones').data('search'));
    setTimeout('TN.tn_src.start()', 3500);
  }
  
  // Inicializa el twitter de menciones de TN.com.ar
  if (($('#twtsearch').length > 0)) {
    TN.tn_src = TN.tw_search('twtsearch', $('#twtsearch').data('width'), $('#twtsearch').data('height'), $('#twtsearch').data('search'));
    setTimeout('TN.tn_src.start()', 3500);
  }  

  // Inicializa el twitter de menciones de TN.com.ar
  TN.init_big_twitter('bigtwitter_cont');
}

TN.init_codas = function() {
  $('#coda-slider-ranking').codaSlider({ dynamicArrows: false, dynamicTabs: false });
  $('#coda-slider-traffic').codaSlider({ dynamicArrows: false, dynamicTabs: false });
  $('#coda-slider-tnyg').codaSlider({ dynamicArrows: false, dynamicTabs: false });
}


TN.init_carousel_photos = function() {

  $('#carousel .content').jCarouselLite({
      btnNext: '#carousel .next',
      btnPrev: '#carousel .prev',
      visible: 6,
      circular: false,
      speed: 1000
  });

  $('#carouseltn .content').jCarouselLite({
      btnNext: '#carouseltn .next',
      btnPrev: '#carouseltn .prev',
      visible: 6,
      circular: false,
      speed: 1000
  });
}

TN.init_carousel_twitter = function() {
  
  if ( $('#carouseltwitter').length > 0) {
    var $c = $('#carouseltwitter');
    var kind = $('#carouseltwitter').data('kind');

    if (kind == 'list') {
      tw_carousel({'id': 'twitbox', 'type': 'list', 'owner': $c.data('twowner'), 'list': $c.data('twlist') });
    } else {
      tw_carousel({'id': 'twitbox', 'type': 'search', 'search': $c.find('.busqueda').data('search')});
    }

    $('.carouseltwitter').jCarouselLite({
      btnNext: '.carouseltwitter .next',
      btnPrev: '.carouseltwitter .prev',
      visible: 3,
      circular: false,
      speed: 1000,
      dynamic: true,
      dyn_item_size: 315,
      dyn_length: 20
    });
  }
}

// Inicializa el twitter de destacadas de portada
TN.init_big_twitter = function(ele) {

  var $e = $('#'+ele);
  if ($e.length === 0) { return; }

  var kind  = $e.data('kind');
  if (kind === 'list') {
    TN.tn_btw = TN.tw_list(ele, $e.data('twowner'), $e.data('twlist'), 950, 380);
    setTimeout('TN.tn_btw.start()', 3500);
  } else if (kind === 'search') {
    TN.tn_btw = TN.tw_search(ele, 950, 380, $e.data('search'));
    setTimeout('TN.tn_btw.start()', 3500);
  }

}

TN.setup_trends = function(srch) {
  TN.tw_search('trend-tag', 600, 2500, $('#trend-tag').data('text')).start();

  TN.init_twitter_widgets();

  $('#coda-slider-ranking').codaSlider({
    dynamicArrows: false,
    dynamicTabs: false
  });

}

TN.setup_trends_home = function(srch) {

  $('#trends-list').jCarouselLite({
      btnNext: '#trends-list .next',
      btnPrev: '#trends-list .prev',
      visible: 1,
      circular: false,
      speed: 1000,
      beforeStart: function(element) {
      	element.find('h2').fadeOut('fast');
      },
      afterEnd: function(element) {
    	  element.find('h2').fadeIn('slow');
      }
  });

  $('#trends-list .first h2').fadeIn('slow');

  TN.init_carousel_twitter();

}


TN.init_analytics_social = function() {
  if ((window.FB === undefined) || (window.twttr === undefined)) {
    setTimeout(TN.init_analytics_social, 500);
    return false;
  }

  FB.Event.subscribe('edge.create', function(targetUrl) {
    _gaq.push(['_trackSocial', 'facebook', 'like', targetUrl]);
  });

  FB.Event.subscribe('edge.remove', function(targetUrl) {
    _gaq.push(['_trackSocial', 'facebook', 'unlike', targetUrl]);
  });

  FB.Event.subscribe('message.send', function(targetUrl) {
    _gaq.push(['_trackSocial', 'facebook', 'send', targetUrl]);
  });

  twttr.events.bind('tweet', function(event) {
    if (event) {
      var targetUrl;
      if (event.target && event.target.nodeName == 'IFRAME') {
        targetUrl = TN.extractParamFromUri(event.target.src, 'url');
      }
      _gaq.push(['_trackSocial', 'twitter', 'tweet', targetUrl]);
    }
  });
}


TN.extractParamFromUri = function(uri, paramName) {
  if (!uri) { return; }
  var uri = uri.split('#')[0];  // Remove anchor.
  var parts = uri.split('?');  // Check for query params.
  if (parts.length == 1) { return; }
  var query = decodeURI(parts[1]);

  // Find url param.
  paramName += '=';
  var params = query.split('&');
  for (var i = 0, param; param = params[i]; ++i) {
    if (param.indexOf(paramName) === 0) { return unescape(param.split('=')[1]); }
  }
}





TN.setup_pulse = function() {

  cplayer('a.cplayer');
  $('a.dplayer').player();
  TN.pulse_toggle();
  TN.pulse_secction();
  //TN.pulse_filter();
  TN.pulse_sort();

};

TN.pulse_contract = function($article, h){
  $article.find('.expanded').fadeOut();
  $article.animate({height: '-=' + h}, 500, function(){
	  $article.find('.contracted').fadeIn();  
	  $article.removeClass('exp');
	  $article.data('status', 'contracted');
  });	
}

TN.pulse_toggle = function(ev){

  $('.pulse-news .toggle').live('click', function(ev){
    
	  $article = $(this).closest('article');
	  
	  if($article.is(':animated')){
	    return false;
	  }
	  var status = $article.data('status');
	  h = '130';

	  if (status == 'expanded'){
		  //contraer
		  TN.pulse_contract($article, h);
	  }
	  
	  if (status == 'contracted'){
		  //contraer todos
		  $exp = $(this).closest('.pulse-news').find('.exp');
		  //$arti = $section.find('[data-status="expanded"]');
		 
		  if($exp.length > 0){
			  TN.pulse_contract($exp, h);
		  }

		  //expandir		  
		  $article.find('.contracted').fadeOut();
		 
		  $article.animate({height: '+=' + h}, 500, function(){
			  $article.find('.expanded').fadeIn();
			  $article.addClass('exp');
			  $article.data('status', 'expanded');
			  $('body').trigger('scroll');
		  });
  
	  }
	  
	  return false;
		  
  });
}

/*
TN.pulse_filter = function(){
  $('#pulse-categories .toggle').click(function(){
    var h = 275;
    var show = 1000;
    var hide = show/2;
    if ($(this).data('status') == 'hide'){
      $(this).parent().animate({height: '+=' + h},show,'swing', 
                    function(){
                      $('#pulse-categories .toggle').data('status', 'show');
                      $(this).addClass('show');
                      }
                     );
    }else{
      TN.pulse_refresh();
      $(this).parent().animate({height: '-=' + h},hide,'swing', 
                      function(){
                      $('#pulse-categories .toggle').data('status', 'hide');
                      $(this).removeClass('show');
                      }
                    );
    }
  });
}
*/


TN.pulse_secction = function (){
  $('#pulse-categories a').click(function(){
    $check = $(this).prev();
    
    if ($check.hasClass('checked')){
      $check.removeClass('checked');
    } else {
      $check.addClass('checked');
    }
    
    TN.pulse_refresh();
    
    return false;
  });    
 }   
 
TN.pulse_refresh = function () { 
    //hacer consulta y cambiar los datos
     $.ajax({'url': TN.BASE_PATH + '/pulse/river-news.html',
            'async': true,
            'type': 'GET',
            'beforeSend': function(){
              $('.pulse-news .loading').fadeIn();
            },
            'success': function(data){
              //$('.pulse-news .pulse-content').html(data)
              
              $('.pulse-news .pulse-content').html(TN.innershiv(data));
                            
              $('.pulse-news .loading').fadeOut();
              cplayer('a.cplayer');
              $('a.dplayer').dplayer();
              $('.pulse-news .pulse-content img').lazyload();
            },
            'error': function(){
              $('.pulse-news .loading').fadeOut();
            }
           });
}

TN.pulse_sort = function() {
  $('.pulse-menu .sort').click(function() {
    if ($(this).data('order') == 'true'){
        order = 'desc';
        $(this).data('order', 'false');
    } else {
        order = 'asc';
        $(this).data('order', 'true');      
    }
    $('.pulse-content article').tsort({attr:'data-' + $(this).data('col'), order: order});
    
    $('.pulse-content article').removeClass('odd').removeClass('even');
    $('.pulse-content article:even').addClass('even');
    $('.pulse-content article:odd').addClass('odd');
    $('body').trigger('scroll');
  });
}

TN.realtime = {}

TN.realtime.content = {}
TN.realtime.sidebar = {}

TN.realtime.content.interval=30000; //in miliseconds
TN.realtime.content.comment_time=4000; //in miliseconds
TN.realtime.content.url=null;
TN.realtime.content.flow = null;

TN.realtime.sidebar.interval=30000; //in miliseconds
TN.realtime.sidebar.comment_time=4000; //in miliseconds
TN.realtime.sidebar.url=null;
TN.realtime.sidebar.flow = null;

TN.realtime.init = function (){
  //aqui debo revisar si existen
  if (  (($('#realtime').length > 0) || ($('#realtime-widget').length > 0)) == false ) {
    return false;
  }
  //url = window.location.protocol + '//'  window.location.pathname;
  url = '/timelines/get?path=' + window.location.pathname.substring(1);
  //console.log('url: ' + url);
  //url = '/api/timeline';
  
  var jqxhr = $.ajax({type: "GET",
                      url: url,
                      dataType: "json",
                      async: true,
                      cache: false
                    });
  
    jqxhr.success(function(data) {
      //console.log('succes');
      if (data == null){
        return false;
      }
      j = 1;
      
      for (i in data)
      {
        
        if ((j == 1) && ($('#realtime').length > 0)){
          TN.realtime.content.url = '/timelines/' + data[i].callback;
          if (data[i].callbackArgs != null) {
            TN.realtime.content.flow = data[i].callbackArgs[1];
          }        
          TN.realtime.content.interval = data[i].interval * 1000;
          TN.realtime.content.get_comment();
        }

        if ((j == 2) && ($('#realtime-widget').length > 0)){
          TN.realtime.sidebar.url = '/timelines/' + data[i].callback;
          if (data[i].callbackArgs != null) {
            TN.realtime.sidebar.flow = data[i].callbackArgs[1];
          }        
          TN.realtime.sidebar.interval = data[i].interval * 1000;
          TN.realtime.sidebar.get_comment();
        }
        j = j + 1;
      }
      
    });  
    
    jqxhr.error(function() {});
  
}

TN.realtime.content.show_comment = function(){
  var $last = $('#realtime .tmp:last');
  
  if ($last.length > 0){
    //show
    //console.log('mostre content')
    $last.animate({opacity: 1,height: $last.find('.comment-container').outerHeight(true)}, 1500, function(){ $(this).removeClass('tmp')} );
  } else {
    clearInterval(TN.realtime.content.timer);
    //console.log('mate content');
  }
}

TN.realtime.sidebar.show_comment = function(){
  var $last = $('#realtime-widget .tmp:last');
  
  if ($last.length > 0){
    //show
    //console.log('mostre sidebar')
    $last.animate({opacity: 1,height: $last.find('.comment-container').outerHeight(true)}, 1500, function(){ $(this).removeClass('tmp')} );
  } else {
    clearInterval(TN.realtime.sidebar.timer);
    //console.log('mate sidebar');
  }
}

TN.realtime.content.get_comment = function(){
  url = TN.realtime.content.url + '/' + $('#realtime .timeago:first').data('timestamp');  

  if (TN.realtime.content.flow != null) {
    url += '/' + TN.realtime.content.flow;
  }
  
  //url = TN.realtime.content.url + '/' + $('#realtime .timeago:first').data('timestamp');
  
  //console.log('url content comments: ' + url);
  //url = 'http://172.16.48.220/tn/site/output/portadas/tnylagente/comunidad/ajax.html'

  var jqxhr = $.ajax({type: "GET",
                      url: url,
                      dataType: "html",
                      async: true,
                      cache: false
                    });
  
  jqxhr.success(function(data) {
    if (data == null){
      
      return false;
    }
    $('#realtime').prepend(data);
    
    setTimeout('TN.realtime.content.set_interval()', 500);
    
  });
}

TN.realtime.sidebar.get_comment = function(){
  url = TN.realtime.sidebar.url + '/' + $('#realtime-widget .timeago:first').data('timestamp');  

  if (TN.realtime.sidebar.flow != null) {
    url += '/' + TN.realtime.sidebar.flow;
  }
  
  //console.log('url sidebar comments: ' + url);
  //url = 'http://172.16.48.220/tn/site/output/portadas/tnylagente/comunidad/ajax.html'

  var jqxhr = $.ajax({type: "POST",
                      url: url,
                      dataType: "html",
                      async: true,
                      cache: false
                    });
  
  jqxhr.success(function(data) {
    if (data == null){
      
      return false;
    }
    $('#realtime-widget').prepend(data);
    
    setTimeout('TN.realtime.sidebar.set_interval()', 500);
    
  });
}

TN.realtime.content.set_interval = function(){
  //console.log('calcule contet');
  i = TN.realtime.content.interval;
  //console.log($('#realtime .tmp').length * TN.realtime.comment_time);

  var total = $('#realtime .tmp').length * TN.realtime.content.comment_time;
  if (total >= TN.realtime.content.interval) {
    i = total; 
  }

  //show the comments
  TN.realtime.content.timer = setInterval('TN.realtime.content.show_comment()', TN.realtime.content.comment_time);
  
  //get the nexts comments
  setTimeout('TN.realtime.content.get_comment()', i);
}

TN.realtime.sidebar.set_interval = function(){
  //console.log('calcule sidebar');
  i = TN.realtime.sidebar.interval;

  var total = $('#realtime-widget .tmp').length * TN.realtime.sidebar.comment_time;
  if (total >= TN.realtime.sidebar.interval) {
    i = total; 
  }

  //show the comments
  TN.realtime.sidebar.timer = setInterval('TN.realtime.sidebar.show_comment()', TN.realtime.sidebar.comment_time);
  
  //get the nexts comments
  setTimeout('TN.realtime.sidebar.get_comment()', i);
}

TN.ajaxBlocks = function() {
  if ( ($('body').hasClass('logged-in') == true)){
    $('body').ajaxBlocks();
  }
}

// @Fx obj {Fixtures}

if(!TN['Fx']) TN['Fx'] = {};

TN.setup_home_fx = function()
{
    cplayer('a.cplayer');
    $('a.dplayer').dplayer();
    window.setTimeout('TN.Fx.init_delayed_home_fixture()', 2000 );
}

TN.Fx.init_delayed_home_fixture = function()
{
  TN.init_twitter_widgets();
  TN.fix_scrapper();
  iepinned();
}   

TN.Fx.fixtureRun = function()
{
    
    // @baseUrl {define the url base}
    var baseUrl = "http://stats.todapasion.tn.com.ar/test/";
    //var baseUrlBanner = "http://172.16.48.104/tn/site/content/images/banners/";
    
    TN.Fx.fixtureMenuControl(baseUrl);
    
    var $path = $("a.active").data("path");
    var $pathBanner = $("a.active").data("banner");
    
    TN.Fx.getContentDefault(baseUrl,$path+"/fixture","fixture19Wrap");
    TN.Fx.getContentDefault(baseUrl,$path+"/goleadores","goleadoresWrap");  
    TN.Fx.getContentDefault(baseUrl,$path+"/posiciones","posicionesWrap");
    TN.Fx.getContentDefault(baseUrl,$path+"/descenso","descensoWrap");
    if($("#fixture-all li a").data("asis") != "false")
    TN.Fx.getContentDefault(baseUrl,$path+"/asistidores","posAsistidores");
    
}

TN.Fx.getDataContext = function(url,path,fixture,list,extend)
{
       $.ajax({ type: "GET",
            url: url+fixture+"/"+path+"/"+list+extend,
            dataType: "html",
            async: true,
            cache: false,
            crossDomain: true,
            success:function(result,i){
            	
            	//console.log(result)
              
              $("#fixture19Wrap #widget-fixture").remove();
              
              /* Prepend data */

              $("#fixture19Wrap").prepend(result);
                 TN.Fx.animMenuControl();
              }
           
          });       
}

TN.Fx.getContentDefault = function(baseUrl,doc,context)
{  
   $("#"+context).load(baseUrl+doc+".html",function(){
        TN.Fx.animMenuControl();
   });
}

TN.Fx.getContentBanner = function(banner)
{
  $(".estadisticas-fixture").prepend("<img src='"+banner+"' width='947' height='113'/>");
}

TN.Fx.animMenuControl = function()
{
    $("li.for, div.for-extend").hover(function(){
                    $("#torneos-fixture").fadeIn("fast"); 
                 },function(){
                    $("#torneos-fixture").fadeOut("fast");  
                 });
}

TN.Fx.fixtureMenuControl = function(baseUrl)
{
   $("#torneos-fixture a").live("click",function(e){
       e.preventDefault();
       
       $this = $(this);
       
       $dataPath = $this.data("path");
       $dataFixture = $this.data("fixture");
       $dataEquipo = $this.data("equipo");

       TN.Fx.getDataContext(baseUrl,$dataPath,$dataFixture,$dataEquipo,".html");
   });
   
   $("#ver-asistidores").live("click",function(e){
       e.preventDefault();
       $("#goleadoresWrap").hide();
       $("#posAsistidores").show();
   });
   
   $("#ver-goleadores").live("click",function(e){
       e.preventDefault();
       $("#posAsistidores").hide();
       $("#goleadoresWrap").show();
   });
}

TN.Fx._exist = function(element,callback){
   var $body = $("body");
   
   this._element = this._element || element;
   
   if(!$body.find(this._element))
       TN.Fx._log("not found {"+this._element+"}");
   else
       TN.Fx._log("Exist {"+this._element+"}");
   if (callback && typeof(callback) === "function") {
        callback.call(this);
    }
}

TN.Fx._log = function(msg){
   //console.log(msg);
}
