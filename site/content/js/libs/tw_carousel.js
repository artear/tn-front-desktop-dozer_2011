
var tw_carousel = function(o) {
  var that = {};

  o = $.extend({'search': ' '}, o);

  var tw = new TWTR.Widget({
    id: o.id,
    type: o.type,
    search: o.search,
    width: 950, height: 534, interval: 4000, subject: "", title: "", rpp: 20, footer: "",
    theme: { shell: { background: '#1deb25', color: '#ffffff' }, tweets: { background: 'inherited', color: 'blue', links: '#0056C3' } },
    features: { avatars: true, hashtags: true, timestamp: true, fullscreen: false, scrollbar: false, live: true, loop: true, behavior: 'default', dateformat: 'absolute', toptweets: true }
  });

  var new_creator = function(o) {
    var t = new tw.original_creator(o);
    var e = document.createElement('li');
    e.className = 'twtr-tweet';
    e.innerHTML = t.element.innerHTML;
    this.element = e;
  };

  tw._getWidgetHtml = function() {
     var html='<div class="content twtr-widget twtr-timeline"><div class="twtr-reference-tweet"></div><ul id="twtr-container" class="twtr-container"></ul></div>';
     return html;
  };

  tw._appendTweet = function(el) {
    var $u = $('#twtr-container');
    if ($u.find('li').length > tw.rpp - 3) {
     $u.css('width', ($u.width()+315)+'px');
     $u.find('li:first').remove();
    };
    $u.append(el);
    $u.parents('section').find('.next').trigger('click');
    return tw;

  };

  tw.set_tweet_creator(new_creator);
  tw.render();
  if (o.type == 'list') { tw.setList(o.owner, o.list); }
  tw.start();
}
