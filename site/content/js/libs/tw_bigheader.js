var tw_bigtwitter = function(o) {
  var that = {};
  o = $.extend({'search': ' '}, o);

  var tw = new TWTR.Widget({
    id: o.id,
    type: o.type,
    search: o.search,
    width: 950, height: 534, interval: 2000, subject: "", title: "", rpp: 20, footer: "",
    theme: { shell: { background: '#1deb25', color: '#ffffff' }, tweets: { background: 'inherited', color: 'blue', links: '#0056C3' } },
    features: { avatars: true, hashtags: true, timestamp: true, fullscreen: false, scrollbar: false, live: true, loop: true, behavior: 'default', dateformat: 'absolute', toptweets: true }
  });    
/*
  tw._old_appendTweet = tw._appendTweet;
  tw._appendTweet = function(el) {
    tw._old_appendTweet(el);
    var $p = $('#' + o.id + ' .twtr-tweet').first().find('p');
    console.log('-' + $p.height()/2);
    $p.css('marginTop', -1 * $p.height()/2);
  };

  tw._appendSlideFade = function(opt_element) {
      var el = opt_element || this.tweet.element;
      this._chop();
      this._appendTweet(el)
      this._slide(el);
      return this;
  };
  */
  tw.render();
  tw.start();
}