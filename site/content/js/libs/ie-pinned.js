function iepinned(){

	if (!($.browser.msie)){
		//no ie
		return false;
	}

	if (parseInt($.browser.version) <= 8) {
		//no 9
		return false;
	}

	if (window.external.msIsSiteMode()) {
        // Site mode is supported and active.
        window.external.msSiteModeClearJumpList();
        window.external.msSiteModeCreateJumpList("Más leidas");	

		$.ajax({
			   type: "GET",
			   url: '/api/most-viewed',
			   dataType: "json",
			   cache: false,
			   success: function(result){
				   
			               var l = result['nodes']['node'].length;
			               var i = 5;
			               
			               if (l < i) {
			            	   i = l;
			               }
	
			               window.external.msSiteModeClearJumpList();
			               for (j = i; i > 0; i--){		            	   
			            	   news = result['nodes']['node'][i-1]
			            	   window.external.msSiteModeAddJumpListItem(news.title, news.url, "http://tn.com.ar/favicon.ico", "self");
			               }
			   }
			});
    } else {
		// Site mode is supported, but inactive.
	}
}
