/**************************************************************************
 Reacciones

 Genera una caja de reacciones sobre un objeto de la pagina y permite
 enviarlo a Facebook o Twitter.

 El marckup para la caja de reacciones debe ser como este:

      <div id="rcbox" class="reacciones_box">
        <h3>Titulo</h3>
        <ul class="reacciones">
          <li><input type="radio" name="random1" rel="asado">Hacer un asado con amigos</li>
          <li><input type="radio" name="random1" rel="asado">Hacer un asado con amigos</li>
          ...
        </ul>
        <div class="redes">
          Compartir con <span class="facebook">Facebook</span> o <span class="twitter">Twitter</span>
        </div>
      </div>


 El parametro 'dom' es el elemento DOM bajo el cual se define la caja de
 reacciones, en el ejemplo 'rcbox'

 El parametro 'data' son las reacciones. se deben definir en formato json
 y el identificador debe ser univoco. ej:

 {
    asado: 'Hacer un asado con Amgios',
    cita: 'Tener una cita',
    cama: 'Quedarce en la cama',
    ...
 }

 La estructura del parametro fb_desc es la estructura a compartir
 en las redes sociales y tiene el siguiente formato:

 {
   name: 'Datos del clima de TN.com.ar para el 9/11',
   description: 'Maxima: 23º ~ Mínima: 17º',
   href: 'http://tn.com.ar/clima',
   media: [{type: 'image', src: 'http://image.src', href: 'http://tn.com.ar/clima'}]
 }

 El campo href sera la url destino a compartir, por ejemplo a ser usada por twitter.
 el resto de los campos, son los requeridos por FB para armar los enlaces.

 version
  - 0.1 - 10/11/2010 - nelson fernandez

***************************************************************************/

TN.reacciones = function(opts) {
  var that = {};
  var data = opts;

  /* inicializa los botones de compartir */
  var setup = function() {

    $(data.dom + ' .redes span').click(function(ev) {
      var $t = $(ev.target);
      var $s = $t.parents('.reacciones_box').find('input:checked')[0];
      if ($s == undefined) {return;}
      $s = $($s);
      if ($t.hasClass('twitter')) {
        var text = ((data.options && data.options.prefix) ? data.options.prefix : '') + data.data[$s.attr('rel')] + escape((data.options && data.options.postfix) ? data.options.postfix : '')
        var ow = window.open('http://twitter.com/share?url='+escape(data.fb_desc.href)+'&text='+ text, '_blank');
        if (ow) { if (ow.focus) {ow.focus()}; return false; }
        ow = null;
      } else if ( $t.hasClass('facebook') ) {
        var text = ((data.options && data.options.prefix) ? data.options.prefix : '') + data.data[$s.attr('rel')] + ((data.options && data.options.postfix) ? data.options.postfix : '')
        TN.FB({message: text, attachment: data.fb_desc.href}).publish();
      }
    });
  };
  that.setup = setup;


  /* Arma la estrucutra de enlaces en base el campo DATA */
  var builDom = function() {
    var $r = $(data.dom + ' .reacciones');
    var rnd = Math.floor(Math.random()*1111);
    $.each(data.data, function(i,e) {
      var $i = $('<input name="reac_'+rnd+'" type="radio">');
      //$r.append( $('<li>').append( $i.attr('name', 'reac_'+rnd).attr('rel', i) ).append('&nbsp;&nbsp;').append(e) );
      $r.append( $('<li>').append( $i.attr('rel', i) ).append('&nbsp;&nbsp;').append(e) );
    });
  };
  that.builDom = builDom;

  return that;
};
