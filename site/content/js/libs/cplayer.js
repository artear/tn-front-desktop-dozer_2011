/*
 * Clarín Player
 * -------------------
 * Necesita:
 * - jQuery 1.5+
 * - jwplayer
 *
 * Copyright © 2011 - Grupo Clarín
 *
 * Version: 1.1
 * Fecha: Junio 2011
 */

var ERROR_URL = 'Problemas para cargar el contenido “{ARCHIVO}”. Revisá tu conexión a Internet';
var ERROR_FLASH = 'Tu versión de Flash es muy antigua. <a href="http://get.adobe.com/es/flashplayer/">Actualizala ahora</a>';
var ERROR_JSON = 'ERROR CRíTICO: El JSON con los datos tiene errores de sintaxis';
var ERROR_OPTIONS = 'ERROR CRíTICO: El JSON con las opciones tiene errores de sintaxis';

var DEBUG = true;

(function($) {

function get_new_id(prefix){
    return prefix + parseInt(Math.random()*11, 10) + new Date().getTime();
}


function cancel(e){
    e.preventDefault();
}

function getFlashVersion(){
    var version = '0,0,0';
    // ie
    try {
        try {
            // avoid fp6 minor version lookup issues
            // see: http://blog.deconcept.com/2006/01/11/getvariable-setvariable-crash-internet-explorer-flash-6/
            var axo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.6');
            try {
                axo.AllowScriptAccess = 'always';
            }
            catch(efv1) {
                return 6;
            }
        }
        catch(efv2) {
        }
        version = new ActiveXObject('ShockwaveFlash.ShockwaveFlash').GetVariable('$version')
            .replace(/\D+/g, ',').match(/^,?(.+),?$/)[1];
    }
    // other browsers
    catch(efv3) {
        try {
            if (navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin){
                version = (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"])
                    .description.replace(/\D+/g, ",").match(/^,?(.+),?$/)[1];
            }
        }
        catch(efv4) {
        }
    }
    return +version.split(',').shift();
}


/* class ClPlayer */
function ClPlayer(trigger, data, options){
    // Opciones por defecto
    // Este diccionario debe quedar definido aqui, dentro de la clase.
    var DEFAULTS = {
    		
    	  isIpad: navigator.userAgent.match(/iPad/i) != null,
        // ¿Mostrar desde el inicio?
        open: false,
        // Nodo a reemplazar (por defecto el que contiene el JSON)
        node: undefined,
        // ¿Ocultar el título si el mouse no está encima del player?
        hideCaption: true,
        // Agregar esta clase al contenedor del player
        className: undefined,

        /* Opciones del carrousel
        *********************************************************** */
        // ¿Activar el pase entre elementos automático?
        autonext: true,
        // Milisegundos entre cambio de imágenes.
        timeout: 3000,
        // Velocidad de la animación.
        speed: 400,
        // Tipo de animación [scrollh, scrollv, fade, fadeb, cover].
        fx: 'scrollh',
        // Función de 'easing' para la animación. Cualquiera instalada.
        easing: 'swing',
        // ¿Ocultar controles del carrousel si el mouse no está encima del player?
        hideCarControls: false,

        /* Opciones del aspecto del jwPlayer
        *********************************************************** */
        // Position of the controlbar. Can be set to bottom, top, over and none.
        controlbar: 'over',
        // If controlbar.position is set to over, this option determines whether
        // the controlbar stays hidden when the player is paused or stopped.
        idlehide: false,
        // Set this to false to show plugin buttons in controlbar.
        // By default (true), plugin buttons are shown in the display.
        dock: true,
        // Set this to false to hide the play button and buffering icons in the display.
        icons: true,
        // Aspecto de los controles multimedia
        skin: '<%= @site.config[:default_skin] %>',

        /* Opciones del comportamiento del jwPlayer
        *********************************************************** */
        autostart: false,
        // Volumen inicial de 0 a 100.
        volume: 80,
        // SOLO FLASH
        // Number of seconds of the file that has to be loaded before the
        // player starts playback. Set this to a low value to enable
        // instant-start (good for fast connections) and to a high value to
        // get less mid-stream buffering (good for slow connections).
        bufferlength: 1,
        // Lista de nombres de plugins separados por comas (ejemplo 'hd,viral').
        plugins: '',
        // ¿Repetir? [none, always]
        repeat: 'none',
        // SOLO FLASH
        // This sets the smoothing of videos, so you won’t see blocks when a
        // video is upscaled. Set this to false to disable the feature and get
        // performance improvements with old computers / big files.
        smoothing: true,
        // Posición en segundos desde donde debe empezar a reproducirse el contenido.
        // Esta opción *solo* funciona con streaming, mp3 y videos de YouTube.
        start: 0,

        /* Marca de Agua
        *********************************************************** */
        // Location of an external JPG, PNG or GIF image to be used as watermark. PNG images with transparency give the best results.
        'logo_file': undefined,
        // HTTP link to jump to when the watermark image is clicked. If it is not set, a click on the watermark does nothing.
        'logo_link': '',
        // Link target for logo click. Can be _self, _blank, _parent, _top or a named frame.
        'logo_linktarget': '_blank',
        // By default, the logo will automatically show when the player buffers and hide 3 seconds later. When this option is set false, the logo will stay visible all the time.
        'logo_hide': true,
        // The distance of the logo, in pixels from the sides of the player.
        'logo_margin': 8,
        // This sets the corner in which to display the watermark. It can be one of the following:
        //     bottom-left
        //     bottom-right
        //     top-left
        //     top-right
        'logo_position': 'top-right',
        // When logo.hide is set to *true*, this option sets the number of seconds the logo is visible after it appears.
        'logo_timeout': 3,
        // The alpha transparency of the logo on mouseover. Can be a decimal number from 0 to 1.
        'logo_over': 0.8,
        // The default alpha transparency of the logo when not moused over. Can be a decimal number from 0 to 1.
        'logo_out': 0.5,

        /* stretching
        *********************************************************** */

        // Defines how to resize the poster image and video to fit the display. Can be:
        //      none: keep the original dimensions.
        //      exactfit: disproportionally stretch the video/image to exactly fit the display.
        //      uniform: stretch the image/video while maintaining its aspect ratio. There’ll be black borders.
        //      fill: stretch the image/video while maintaining its aspect ratio, completely filling the display.
        stretching: 'uniform',


        flashplayer: '<%= @site.config[:default_swf] %>'
    };

    this.$trigger = $(trigger);

    if (typeof(data) === 'string'){
        try {
            data = $.parseJSON(data);
        }
        catch(e1){
            this.show_error('ERROR_JSON', data);
            return;
        }
    }

    if (typeof(options) === 'string'){
        try {
            options = $.parseJSON(options);
        }
        catch(e2){
            this.show_error('ERROR_OPTIONS', options);
            return;
        }
    }

    this.op = typeof(options) === undefined
        ? DEFAULTS
        : $.extend(DEFAULTS, options);
    this.op.open = this.op.open && this.op.open !== 'false';
    this.op.autoplay = this.op.autoplay && this.op.autoplay !== 'false';

    this.$replace = this.$trigger;
    if (this.op.node){
    this.$replace = this.$trigger.parents(this.op.node);
        if (this.$replace.length === 0){
            this.$replace = $(this.op.node);
        }
    }

    this.items = data.items;
    this.num_items = data.items.length;
    this.index = 0;

    this.ping_open = data.ping_open;
    this.ping_close = data.ping_close;

    var self = this;
    this.$trigger.click(function(e){ self.open(e); });
    if (this.op.open){
        this.$trigger.trigger('click');
    }
}

ClPlayer.prototype.open = function(e){
    var maxwidth = 0;
    var maxheight = 0;
    var i, item;
    this.create_container();

    // Render items
    for (i = 0; item = this.items[i]; i += 1){
        maxwidth = Math.max(maxwidth, item.width || 0);
        maxheight = Math.max(maxheight, item.height || 0);
    }
    maxwidth = Math.ceil(maxwidth);
    maxheight = Math.ceil(maxheight);
    this.$root.css({width: maxwidth, height: maxheight});
    this.maxwidth = maxwidth;
    this.maxheight = maxheight;
    for (i = 0; item = this.items[i]; i += 1){
        this['render_' + item.media_type](item);
    }

    this.load();

    if (this.num_items > 1){
        if (this.op.autonext){
            this.car_play();
        }
        else {
            this.car_pause();
        }
    }

    if (this.ping_open){
        this.send_signal('open');
    }
};

ClPlayer.prototype.close = function(e){
    if (this.ping_open){
        this.send_signal('close');
    }
    clearTimeout(this.t_autoplay);
    this.index = 0;
    this.$replace.removeClass('hide');
    this.$root.remove();
};

ClPlayer.prototype.create_container = function(){
    var self = this;
    this.id = get_new_id('cp-');
    this.$root = $('<div class="cplayer">').attr('id', this.id);
    this.$items = $('<div class="cp-items">').appendTo(this.$root);
    
    if (this.op.isIpad == true){
    	this.$root.addClass('cp-ipad');
    }

    this.$replace.addClass('hide');
    this.$root.insertBefore(this.$replace);

    if (this.op.className !== undefined){
        this.$root.addClass(this.op.className);
    }

    if (! this.op.open){
        $('<a class="cp-close" tabindex="1" title="Cerrar">&times;</a>')
            .click(function(e){ self.close(e); })
            .appendTo(this.$root);
        this.$root.addClass('cp-ispop');
    }

    this.$error_msg = $('<div class="cp-error">').hide().appendTo(this.$root);

    this.has_media = false;
    this.is_carrousel = false;

    if (this.num_items > 1){
        this.is_carrousel = true;

        var $car_controls = $('<div class="cp-car-controls">')
            .appendTo(this.$root);

        this.$cp_index = $('<span class="cp-index-item">')
            .appendTo($car_controls);

        $('<a class="cp-prev-item" tabindex="1" title="Anterior">prev</a>')
            .click(function(e){
                self.show_prev();
                e.preventDefault();
            })
            .dblclick(cancel)
            .appendTo($car_controls);

        $('<a class="cp-next-item" tabindex="1" title="Siguiente">next</a>')
            .click(function(e){
                self.show_next();
                e.preventDefault();
            })
            .dblclick(cancel)
            .appendTo($car_controls);

        this.$car_play = $('<a class="cp-carplay" tabindex="1">play</a>')
            .click(function(e){
                self.car_play();
                e.preventDefault();
            })
            .dblclick(cancel)
            .appendTo($car_controls);

        this.$car_pause = $('<a class="cp-carpause" tabindex="1">pause</a>')
            .click(function(e){
                self.car_pause();
                e.preventDefault();
            })
            .dblclick(cancel)
            .appendTo($car_controls);

        this.$car_controls = $car_controls;
        this.$root.addClass('cp-carrousel');
    }

    this.$caption = $('<div class="cp-item-caption"></div').appendTo(this.$root);
    this.setup_fade();
};

ClPlayer.prototype.setup_fade = function(){
    var self = this;
    var FADE_IN_SPEED = 200;
    var FADE_OUT_SPEED = 300;

    this.$root.hover(
        function(){
            if (self.is_carrousel && self.op.hideCarControls){
                self.$car_controls.fadeIn(FADE_IN_SPEED);
            }
            if (self.op.hideCaption){
                self.$caption.fadeIn(FADE_IN_SPEED);
            }
        },
        function(){
            if (self.is_carrousel && self.op.hideCarControls){
                self.$car_controls.fadeOut(FADE_OUT_SPEED);
            }
            if (self.op.hideCaption){
                self.$caption.fadeOut(FADE_OUT_SPEED);
            }
        }
    );
    if (this.op.open){
        this.$root.trigger('mouseleave');
    }
};

ClPlayer.prototype.add_media = function(){
    if ($.browser.msie && parseInt($.browser.version, 10) < 9 && getFlashVersion() < 9){
        this.show_error('ERROR_FLASH');
        this.has_media = true;
        return;
    }

    this.$media = $('<div class="cp-media hide">')
        .css({
            width: this.$root.width(),
            height: this.$root.height()
        })
        // The media player must be behind the error message.
        .insertBefore(this.$error_msg);

    this.has_media = true;

    // jwPlayer modifica el estilo del elemento que se le pasa,
    // por eso debo crear uno dentro de cp-media.
    var $mpl = $('<div>').attr('id', get_new_id('cpm-'))
        .appendTo(this.$media);
    this.jwplayer = jwplayer($mpl[0]);
};

ClPlayer.prototype.render_image = function(data){
    var url = data.url;
    if (url[0] === '#'){
        url = $(url).attr('src');
    }
    var $item = $('<div class="cp-item cp-image hide">');

    var css = {position: 'absolute'};
    if (data.width){
        css.left = Math.max((this.maxwidth - data.width) >> 1, 0);
        css.width = data.width;
    }
    if (data.height){
        css.top = Math.max((this.maxheight - data.height) >> 1, 0);
        css.height = data.height;
    }

    $('<img alt="" class="cp-item-img" />')
        .attr('src', url)
        .css(css)
        .appendTo($item);

    $item.appendTo(this.$items);
    data.$item = $item;
};

ClPlayer.prototype.render_video = function(data){
    if (!this.has_media){
        this.add_media();
    }

    var $item = $('<div class="cp-item cp-video hide">');

    if (data.cover){
        var css = {position: 'absolute'};
        if (data.width){
            css.left = Math.max((this.maxwidth - data.width) >> 1, 0);
            css.width = data.width;
        }
        if (data.height){
            css.top = Math.max((this.maxheight - data.height) >> 1, 0);
            css.height = data.height;
        }

        $('<img alt="" class="cp-item-img" />')
            .attr('src', data.cover)
            .css(css)
            .appendTo($item);
    }

    $item.appendTo(this.$items);
    data.$item = $item;
};

ClPlayer.prototype.render_audio = function(data){
    if (!this.has_media){
        this.add_media();
    }

    var $item = $('<div class="cp-item cp-audio hide">');

    if (data.cover){
        var css = {position: 'absolute'};
        if (data.width){
            css.left = Math.max((this.maxwidth - data.width) >> 1, 0);
            css.width = data.width;
        }
        if (data.height){
            css.top = Math.max((this.maxheight - data.height) >> 1, 0);
            css.height = data.height;
        }

        $('<img alt="" class="cp-item-img" />')
            .attr('src', data.cover)
            .css(css)
            .appendTo($item);
    }

    $item.appendTo(this.$items);
    data.$item = $item;
};

ClPlayer.prototype.load = function(autostart){
	
    autostart = (typeof(autostart) === undefined) ? false : autostart;

    this.$error_msg.hide();

    var data = this.items[this.index];
    data.$item.removeClass('hide');

    if (this.$cp_index){
        this.$cp_index.text((this.index + 1) + ' de ' + this.num_items);
    }

    if (data.caption){
        this.$caption.html('<span>' + data.caption + '</span>').removeClass('hide');
    }
    else {
        this.$caption.addClass('hide');
    }

    if (this.has_media){
        this.$media.addClass('hide');
        this.jwplayer.remove();
    }

    if (data.media_type === 'image'){
        return;
    }

    /* Video & audio solamente, de aquí en adelante */

    var self = this;
    this.$root.removeClass('cp-image').removeClass('cp-video')
        .removeClass('cp-audio').addClass('cp-' + data.media_type);

    this.setup_signals(data);

    clearTimeout(this.t_mediaload);

    var config = {
        'flashplayer': this.op.flashplayer,

        'width': this.maxwidth,
        'height': this.maxheight,
        'image': data.cover,

        'controlbar': this.op.controlbar,
        'controlbar.idlehide': this.op.idlehide,
        'dock': this.op.dock,
        'icons': this.op.icons,
        'skin': this.op.skin,
        'autostart': this.op.autostart,
        'volume': this.op.volume,
        'bufferlength': this.op.bufferlength,
        'plugins': this.op.plugins,
        'repeat': this.op.repeat,
        'smoothing': this.op.smoothing,
        'start': this.op.start,
      
        /*
        'logo.file': this.op.logo_file,
        'logo.link': this.op.logo_link,
        'logo.linktarget': this.op.logo_linktarget,
        'logo.hide': this.op.logo_hide,
        'logo.margin': this.op.logo_margin,
        'logo.position': this.op.logo_position,
        'logo.timeout': this.op.logo_timeout,
        'logo.over': this.op.logo_over,
        'logo.out': this.op.logo_out,
    	*/         
        
        'stretching': this.op.stretching,

        'events': self.setup_signals(data)
    };   
    
    //var isIpad = navigator.userAgent.match(/iPad/i) != null;
    
    if ( (data.levels !== undefined) && (this.op.isIpad == false) ){
        config.levels = data.levels;
    }
    if (data.url !== undefined){
        config.file = data.url;
    }    	

    if (data.streamer !== undefined){
        config.streamer = data.streamer;
    }
    if (data.provider !== undefined){
        config.provider = data.provider;
    }
    
    if (data.modes !== undefined){
    	config.modes = data.modes;
    	for (i in config.modes) {
    		if (config.modes[i].type == 'flash'){
    			config.modes[i].src = this.op.flashplayer;
    		}
    		if (config.modes[i].type == 'html5'){
    			tmpfile = config.modes[i].config.file;
    		}
    	}
    	if (this.op.isIpad == true){
    		config.file = tmpfile.replace("/playlist.m3u8","");
    	}
    }
    
    if (this.op.logo_file !== undefined) {
    	
    	var watermark = {
    	        'logo.file': this.op.logo_file,
    	        'logo.link': this.op.logo_link,
    	        'logo.linktarget': this.op.logo_linktarget,
    	        'logo.hide': this.op.logo_hide,
    	        'logo.margin': this.op.logo_margin,
    	        'logo.position': this.op.logo_position,
    	        'logo.timeout': this.op.logo_timeout,
    	        'logo.over': this.op.logo_over,
    	        'logo.out': this.op.logo_out			
    	}
    	/* merge watermark into config */
    	$.extend(config, watermark);    	
    }

    if (data['ova.json'] !== undefined){
        config['ova.json'] = data['ova.json'];
    }
    else if (data['ova.xml'] !== undefined){
        config.config = data['ova.xml'];
    }
    else if (data.config !== undefined){
        config.config = data.config;
    }
    
    //console.log(config)
    
    this.t_mediaload = setTimeout(function(){
        self.jwplayer.setup(config);
        self.$media.removeClass('hide');
    }, 500);
};

ClPlayer.prototype.setup_signals = function(data){
    var self = this;
    var events = {
        onReady: function(event){
            self.$media.removeClass('hide');
        },
        onError: function(event){
            if (DEBUG && (typeof(console) !== 'undefined') && console.log){
                console.log(event);
            }
            self.$media.addClass('hide');
            self.show_error(event.message);
        }
    };
    
    if (data.levels !== undefined){
  		events.onFullscreen = function(event) {
  			self.jwplayer.seek(self.jwplayer.getPosition());
  		};        	
    }    

    if (data.ping_play){
         events.onPlay = function(event){
            self.send_signal('PLAY', data.ping_play, event);
        };
    }
    if (data.ping_pause){
        events.onPause = function(event){
            self.send_signal('PAUSE', data.ping_pause, event);
        };
    }
    if (data.ping_complete){
        events.onComplete = function(event){
            self.send_signal('COMPLETE', data.ping_complete, event);
        };
    }
    if (data.ping_volume){
        events.onVolume = function(event){
            self.send_signal('VOLUME', data.ping_volume, event);
        };
    }
    
    if (data.ping_fullscreen){
        events.onFullscreen = function(event){
            self.send_signal('FULLSCREEN', data.ping_fullscreen, event);
        };
    }
	
    return events;
};

ClPlayer.prototype.send_signal = function(name, url, data){
    if (DEBUG && (typeof(console) !== undefined) && console.log){
        console.log('EVENTO '+ name);
        console.log(data);
    }
};

ClPlayer.prototype.show_error = function(error){
    switch(error){
        // Errores críticos.
        case 'ERROR_FLASH':
            $('<p class="cp-critical-error">' + ERROR_FLASH + '</p>').insertBefore(this.$replace);
            return;
        case 'ERROR_JSON':
            $('<p class="cp-critical-error">' + ERROR_JSON + '</p>').insertBefore(this.$replace);
            return;
        case 'ERROR_OPTIONS':
            $('<p class="cp-critical-error">' + ERROR_OPTIONS + '</p>').insertBefore(this.$replace);
            return;
    }

    var self = this;
    var $retry = $('<a href="">¿Reintentar?</a>')
        .mousedown(function(){
            self.load(true);
        });
    var $msg;

    switch(error.slice(0, 32)){
        // Errores ¿recuperables?
        case 'Video not found or access denied':
        case 'Error #2032':
            var data = this.items[this.index];
            $msg = $('<p>' + ERROR_URL.replace('{ARCHIVO}', data.url) + '<br></p>').append($retry);
            break;

        default:
            $msg = error;
    }
    
    if (this.$error_msg){
        this.$error_msg.html('').append($msg).show();
    }
};


/* * CARROUSEL ************************************************************* */

ClPlayer.prototype.show_prev = function(){
    this.$error_msg.hide();
    if (this.curr_anim){
        this.curr_anim.stop(false, true);
    }
    if (this.has_media){
        this.$media.addClass('hide');
        // Cancelar la carga del video/audio en el media player
        clearTimeout(this.t_mediaload);
        try {
            this.jwplayer.stop();
        }
        catch(e) {}
    }

    var curr = this.items[this.index];
    var next = this.items[this.index - 1];

    if (next === undefined){
        this.index = this.num_items - 1;
        next = this.items[this.index];
    }
    else {
        this.index -= 1;
    }
    var $curr = curr.$item;
    var $next = next.$item;

    switch(this.op.fx){
        case 'scrollh':
            this.anim_scrollh_lr($curr, $next, this.car_check);
            break;
        case 'scrollv':
            this.anim_scrollh_bt($curr, $next, this.car_check);
            break;
        case 'fade':
            this.anim_fade($curr, $next, this.car_check);
            break;
        case 'fadeb':
            this.anim_fadeb($curr, $next, this.car_check);
            break;
        case 'cover':
            this.anim_cover($curr, $next, this.car_check);
            break;
        default:
            this.anim_scrollh_lr($curr, $next, this.car_check);
            break;
    }
};

ClPlayer.prototype.show_next = function(){
    this.$error_msg.hide();
    if (this.curr_anim){
        this.curr_anim.stop(false, true);
    }
    if (this.has_media){
        this.$media.addClass('hide');
        // Cancelar la carga del video/audio en el media player
        clearTimeout(this.t_mediaload);
        try {
            this.jwplayer.stop();
        }
        catch(e) {}
    }

    var curr = this.items[this.index];
    var next = this.items[this.index + 1];

    if (next === undefined){
        next = this.items[0];
        this.index = 0;
    }
    else {
        this.index += 1;
    }
    var $curr = curr.$item;
    var $next = next.$item;

    switch(this.op.fx){
        case 'scrollh':
            this.anim_scrollh_rl($curr, $next, this.car_check);
            break;
        case 'scrollv':
            this.anim_scrollh_tb($curr, $next, this.car_check);
            break;
        case 'fade':
            this.anim_fade($curr, $next, this.car_check);
            break;
        case 'fadeb':
            this.anim_fadeb($curr, $next, this.car_check);
            break;
        case 'cover':
            this.anim_uncover($curr, $next, this.car_check);
            break;
        default:
            this.anim_scrollh_rl($curr, $next, this.car_check);
            break;
    }
};

ClPlayer.prototype.car_play = function(){
    this.$car_play.hide();
    this.$car_pause.show();
    this.autoplaying = true;
    var self = this;
    // Para cancelar el cambio si manualmente se ha hecho.
    clearTimeout(this.t_autoplay);
    this.t_autoplay = setTimeout(function(){
        self.show_next();
    }, this.op.timeout);
};

ClPlayer.prototype.car_pause = function(){
    this.$car_play.show();
    this.$car_pause.hide();
    this.autoplaying = false;
    clearTimeout(this.t_autoplay);
};

ClPlayer.prototype.car_check = function(){
    if (this.autoplaying){
        this.car_play();
    }
    var data = this.items[this.index];
    if (data.ping_show){
        this.send_signal('show');
    }
    this.load();
};


ClPlayer.prototype.anim_scrollh_lr = function($curr, $next, callback){
    $next.css({
        top: $curr.css('top'),
        left: parseInt($curr.css('left'), 10) - $next.width()
    }).removeClass('hide');

    var self = this;
    this.curr_anim = $curr.add($next).animate({
            'left': '+=' + $curr.width()
        },
        this.op.speed,
        this.op.easing,
        function(){
            $curr.addClass('hide');
            self.curr_anim = null;
            callback.apply(self);
        }
        );
};

ClPlayer.prototype.anim_scrollh_rl = function($curr, $next, callback){
    $next.css({
        top: $curr.css('top'),
        left: parseInt($curr.css('left'), 10) + $curr.width()
    }).removeClass('hide');

    var self = this;
    this.curr_anim = $curr.add($next).animate({
            'left': '-=' + $curr.width()
        },
        this.op.speed,
        this.op.easing,
        function(){
            $curr.addClass('hide');
            self.curr_anim = null;
            callback.apply(self);
        });
};

ClPlayer.prototype.anim_scrollh_tb = function($curr, $next, callback){
    $next.css({
        top: parseInt($curr.css('top'), 10) - $next.height(),
        left: $curr.css('left')
    }).removeClass('hide');

    var self = this;
    this.curr_anim = $curr.add($next).animate({
            'top': '+=' + $curr.height()
        },
        this.op.speed,
        this.op.easing,
        function(){
            $curr.addClass('hide');
            self.curr_anim = null;
            callback.apply(self);
        }
        );
};

ClPlayer.prototype.anim_scrollh_bt = function($curr, $next, callback){
    $next.css({
        top: parseInt($curr.css('top'), 10) + $curr.height(),
        left: $curr.css('left')
    }).removeClass('hide');

    var self = this;
    this.curr_anim = $curr.add($next).animate({
            'top': '-=' + $curr.height()
        },
        this.op.speed,
        this.op.easing,
        function(){
            $curr.addClass('hide');
            self.curr_anim = null;
            callback.apply(self);
        });
};

ClPlayer.prototype.anim_uncover = function($curr, $next, callback){
    $curr.css('zIndex', 1);
    $next.css({
        top: $curr.css('top'),
        left: $curr.css('left'),
        zIndex: 0
    }).removeClass('hide');

    var self = this;
    this.curr_anim = $curr.animate({
            'left': '-=' + $curr.width()
        },
        this.op.speed,
        this.op.easing,
        function(){
            $curr.css('zIndex', 0).addClass('hide');
            self.curr_anim = null;
            callback.apply(self);
        }
        );
};

ClPlayer.prototype.anim_cover = function($curr, $next, callback){
    $curr.css('zIndex', 0);
    $next.css({
        top: $curr.css('top'),
        left: parseInt($curr.css('left'), 10) - $next.width(),
        zIndex: 1
    }).removeClass('hide');

    var self = this;
    this.curr_anim = $next.animate({
            'left': '+=' + $next.width()
        },
        this.op.speed,
        this.op.easing,
        function(){
            $curr.addClass('hide');
            $next.css('zIndex', 0);
            self.curr_anim = null;
            callback.apply(self);
        }
        );
};

ClPlayer.prototype.anim_fade = function($curr, $next, callback){
    $curr.css('zIndex', 1);
    $next.css({
        top: $curr.css('top'),
        left: $curr.css('left'),
        zIndex: 0
    }).removeClass('hide');

    var self = this;
    this.curr_anim = $curr.animate({
            'opacity': 0
        },
        this.op.speed,
        this.op.easing,
        function(){
            $curr.addClass('hide').css({'zIndex': 0, 'opacity': 1});
            self.curr_anim = null;
            callback.apply(self);
        });
};

ClPlayer.prototype.anim_fadeb = function($curr, $next, callback){
    // para contrarrestar los efectos de animaciones paradas antes de tiempo
    $curr.css({
        zIndex: 1,
        opacity: 1
    }).removeClass('hide');

    $next.css({
        top: $curr.css('top'),
        left: $curr.css('left'),
        zIndex: 0,
        opacity: 0
    }).removeClass('hide');

    var self = this;
    var speed_2 = parseInt(this.op.speed / 2, 10);

    this.curr_anim = $curr.animate({
            'opacity': 0
        },
        speed_2,
        this.op.easing,
        function(){
            $curr.addClass('hide').css({'zIndex': 0, 'opacity': 1});
            $next.animate({
                'opacity': 1
            },
            speed_2,
            self.op.easing,
            function(){
                self.curr_anim = null;
            });
            self.curr_anim = null;
            callback.apply(self);
        });
};

/* **************************************************************** */

window.ClPlayer = ClPlayer;
window.cplayer = function(sel){
    $(sel).each(function(){
        var json = this.getAttribute('data-json') || {};
        var options = this.getAttribute('data-op') || {};
        var player = new ClPlayer(this, json, options);
        $.data(this, 'player', player);
    });
};

}(jQuery));
