
function starcarousel(){
	$('#banner-container').jCarouselLite({
		speed: 700, //tiempo de la transicion
		auto: 7000, //tiempo que se muestra la imagen
		visible:1,
		circular: true,
		btnGo:
		    ["#navigation-tabs .tab-1", "#navigation-tabs .tab-2", "#navigation-tabs .tab-3",
		     "#navigation-tabs .tab-4", "#navigation-tabs .tab-5", "#navigation-tabs .tab-6", "#navigation-tabs .tab-7"],
		beforeStart: function(a) {
	        $('#banner-container').addClass('top');
	        $('#navigation-tabs li.' + $(a).attr('class')).removeClass('current');
	    },
	    afterEnd: function(a) {
	    	$('#banner-container').removeClass('top');
	    	$('#navigation-tabs li.' + $(a).attr('class')).addClass('current');
	    	$('#banner-container .tab-1 img').css('opacity', '1');
	    }
	});
}


function precarousel(){
	$('#banner-container .tab-1 img').ready(function () {
		$('#banner-container .tab-1 img').fadeIn('slow');
		$('#tnmovil').fadeOut('slow');
		starcarousel();
	}).attr('src', $('#banner-container .tab-1 img').attr('src'));
}

function firstfade(){
	$('#black').ready(function () {
		$('#black').fadeOut('fast');
		setTimeout(	"precarousel()", 2000);
	});
}


$(document).ready(function(){
	setTimeout(	"firstfade()", 2000);
});
