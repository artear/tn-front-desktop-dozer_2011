if ( typeof window.JSON === 'undefined' ) {
  $LAB.script(TN.BASE_PATH + 'js/libs/json2.js');
}

TN_startup = function() {
  if (TN.warming_up === true) { return; };
  TN.warming_up = true;
  $.each(TN.initfuncs, function(i,e) {
    TN[e].call();
  });
}

$LAB.script(TN.BASE_PATH + 'js/libs/jquery-164.js').wait()
	.script(TN.BASE_PATH + 'js/libs/twitter-widget-min.js')
	.script(TN.BASE_PATH + 'js/libs/ie-pinned.js')
	.script(TN.BASE_PATH + 'js/libs/tn.js')
	.script(TN.BASE_PATH + 'js/plugins/ajaxblocks.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-lazyload.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-cookie.js')
    .script(TN.BASE_PATH + 'js/zinit.js')
    .script(TN.BASE_PATH + 'js/libs/cplayer.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-dplayer.js')
    .script(TN.BASE_PATH + 'js/libs/jwplayer.js')
    .script(TN.BASE_PATH + 'js/libs/tn_fb.js')
    .script(TN.BASE_PATH + 'js/libs/tw_carousel.js')
    .script(TN.BASE_PATH + 'js/libs/tw_bigheader.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery.tools.min.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-eplanning.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-coda-slider-2.0.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-easing.1.3.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-keycounter.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-isotope-min.js')
    .script(TN.BASE_PATH + 'js/plugins/jcarousellite.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-tinysort-min.js')
    .script(TN.BASE_PATH + 'js/plugins/tn-history.js')
    .script('http://platform.twitter.com/widgets.js')
    .wait(function() { 
    	plupload();
    	first_load(); 
    	TN_startup(); 
    	
    });

//    .script('https://apis.google.com/js/plusone.js')

if (head.browser.ie)  {
	if (parseInt(head.browser.version) <= 6)   {
		$LAB.script(TN.BASE_PATH + 'js/libs/dd_belatedpng.js',TN.BASE_PATH + 'js/libs/pngfix.js');
	}
}

var pattern = /ckeditor/g;
if (pattern.test(document.body.className) == true) {
	$LAB.script(TN.BASE_PATH + 'ckeditor/ckeditor.js').wait()
	    .script(TN.BASE_PATH + 'js/libs/ckeditor-config.js');
}

function plupload(){
	var pattern = /pupload/g;
	if (pattern.test(document.body.className) == true) {
		$LAB.script(TN.BASE_PATH + 'js/plugins/drupal.js')
			.script(TN.BASE_PATH + 'js/plugins/autocomplete.js')
			.script(TN.BASE_PATH + 'js/plugins/jquery-form-min.js')
			.script(TN.BASE_PATH + 'js/libs/plupload-min.js')
			.script(TN.BASE_PATH + 'js/libs/plupload-flash-min.js')
		    .script(TN.BASE_PATH + 'js/plugins/jquery-plupload-queue-min.js').wait()
		    .script(TN.BASE_PATH + 'js/libs/plupload-config.js');	
	}
}
/*

.script(TN.BASE_PATH + 'js/plugins/tabledrag.js')

.script(TN.BASE_PATH + 'js/plugins/ahah.js')
*/	
