if ( typeof window.JSON === 'undefined' ) {
  $LAB.script(TN.BASE_PATH + 'js/libs/json2.js');
}

TN_startup = function() {
  if (TN.warming_up === true) { return; };
  TN.warming_up = true;
  $.each(TN.initfuncs, function(i,e) {
    TN[e].call();
  });
}

$LAB.script(TN.BASE_PATH + 'js/libs/jquery-164_min.js').wait()
    .script(TN.BASE_PATH + 'js/prod/tn.js')
    .script('http://platform.twitter.com/widgets.js')
    .wait(function() { plupload(); first_load(); TN_startup(); });

if (head.browser.ie)  {
	if (parseInt(head.browser.version) <= 6)   {
		$LAB.script(TN.BASE_PATH + 'js/libs/dd_belatedpng.js',TN.BASE_PATH + 'js/libs/pngfix.js');
	}
}

var pattern = /ckeditor/g;
if (pattern.test(document.body.className) == true) {
	$LAB.script(TN.BASE_PATH + 'ckeditor/ckeditor.js').wait()
	    .script(TN.BASE_PATH + 'js/libs/ckeditor-config.js');
}

function plupload(){
	var pattern = /pupload/g;
	if (pattern.test(document.body.className) == true) {
		$LAB.script(TN.BASE_PATH + 'js/prod/publish-min.js')
	}
}