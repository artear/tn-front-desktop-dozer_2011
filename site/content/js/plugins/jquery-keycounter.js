// Contador de caracteres genérico para un textarea
//
//  $('textarea').keycounter();
//  usas los valores por defecto.. el nombre del contador es 'counter' y 140 caracteres máximos
//
//  $('textarea').keycounter({'counter': '#area_counter', 'max': 300});
//  el contador va a ser el elemento area_counter y tendrá un máximo de 300 caracteres
//
//  Nelson Fernandez - 23.5.2011 - v.0.1
//
(function($) {
  var m = {};
  m.o = {};
  
  m.keydown = function(ev) {
    if ((ev.keyCode == 8) || (ev.keyCode == 9) || ((ev.keyCode >= 35) && (ev.keyCode <= 46))) { return; }
    if (m.o.t.val().length < m.o.max) {
      m.o.c.html(m.o.t.val().length);
      return true;
    } else {
      ev.stopPropagation();
      return false;
    }
  };

  m.keyup = function(ev) {
    m.o.c.html(m.o.t.val().length);

    if (m.o.t.val().length > m.o.max) {
    	//mas de X
    	if (m.o.is_enabled == true){
    		//button is enabled, we going to disabled
    		m.o.c.addClass(m.o.error);
    		m.o.button.addClass('disabled');
    		m.o.button.attr('disabled', true);
    		m.o.is_enabled = false;
    	}
    } else {
    	//menos de X
    	if (m.o.is_enabled == false){
    		m.o.c.removeClass(m.o.error);
    		m.o.button.removeClass('disabled');
    		m.o.button.removeAttr('disabled');
    		m.o.is_enabled = true;
    	}
    	
    }
  };

  $.fn.keycounter = function(options) {
    m.o = $.extend({ 'counter': '#counter', 'max': 140, 'button': '.submit', 'error': 'red' }, options);
    m.o.t = $(this);
    m.o.c = $(m.o.counter);
    m.o.button = $(m.o.button);
    m.o.is_enabled = true; 
    return this.each( function() {
      $(this).bind('keydown', m.keydown);
      $(this).bind('keyup', m.keyup);
    });
  };
  
})(jQuery);
