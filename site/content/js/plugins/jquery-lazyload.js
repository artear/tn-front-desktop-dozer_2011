/*
 * Lazy Load - jQuery plugin for lazy loading images
 *
 * Copyright (c) 2007-2009 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   http://www.appelsiini.net/projects/lazyload
 *
 * Version:  1.5.0
 *
 */
(function($) {

    $.fn.lazyload = function(options) {
        var settings = {
            threshold    : 0,
            placeholder  : '',
            effect       : "show",
            container    : window,
            appear       : undefined,
            failurelimit : undefined
        };

        if(options) {
            $.extend(settings, options);
        }
        settings.container = (settings.container === undefined ? window : settings.container);

        var appear = function() {
          var self = this;
          if (!this.loaded) {
              $('<img />')
                  .bind('load', function() {
                      $(self).hide()
                          .attr('src', $(self).attr('data-src'))
                          .removeAttr('data-src')
                          [settings.effect](settings.effectspeed);
                      self.loaded = true;
                  })
                  .attr('src', $(self).attr('data-src'));
          };
        }

        this.each(function() {
            var self = this;

            /* Save data-src only if it is not defined in HTML. */
            if (undefined == $(self).attr('data-src')) {
                $(self).attr('data-src', $(self).attr('src'));
            }
            //$(self).attr('src', settings.placeholder);
            self.loaded = false;

            if (settings.appear === undefined) {
                var app =  jQuery.proxy(appear, this);
            }
            else {
                var app = settings.appear;
            };
            /* When appear is triggered load data-src image. */
            $(self).one('appear', app);
        });

        var elements = this;
        var onScroll = function(e) {
            var counter = 0;
            elements.each(function() {
                counter += 1;
                if (settings.failurelimit && counter > settings.failurelimit){
                    return false;
                }
                if ($.insideview(this, settings)) {
                    $(this).trigger('appear');
                }
            });
            /* Remove image from array so it is not looped next time. */
            var temp = $.grep(elements, function(element) {
                return !element.loaded;
            });
            elements = $(temp);
        }

        $(settings.container).bind('scroll', onScroll);
        $('body').trigger('scroll');
        /* Force initial check if images should appear. */
        return this;
    };

    /* Convenience methods in jQuery namespace.           */
    /* Use as  $.insideview(element, {threshold : 100, container : window}) */

    $.insideview = function(element, settings) {
        var eOffset = $(element).offset();
        var eTop = eOffset.top;
        var eLeft = eOffset.left;

        if (settings.container !== window) {
            var $container = $(settings.container);
            var cOffset = $container.offset();
            eTop -= cOffset.top;
            eLeft -= cOffset.left;
        }
        else {
            var $container = $(window);
        }

        var cWidth = $container.width();
        var cHeight = $container.height();
        var scrollTop = $container.scrollTop();
        var scrollLeft = $container.scrollLeft();

        return (
            (eTop < (cHeight + scrollTop)) && (eTop > scrollTop) &&
            (eLeft < (cWidth + scrollLeft)) && (eLeft > scrollLeft)
            )
    };

    /* Custom selector for your convenience.   */
    /* Use as $("img:inside-view").something() */
    $.extend($.expr[':'], {
        "inside-view" : "$.insideview(a, {threshold : 0, container: window})"
    });

})(jQuery);
