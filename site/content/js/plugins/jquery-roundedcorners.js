jQuery.fn.roundedcorners = function(){
	
	if (parseInt(jQuery.browser.version) <=8 ) {
		
	    this.each( function(){
	    	
			var background = new Array('color', 'attachment', 'image', 'position', 'repeat'); 
			var border = new Array('color', 'style', 'width');
			
			var regexp = new RegExp("[0-9]*");
			var thisheight = Number(regexp.exec($(this).css('height')))
			var newheight =  thisheight + Number(regexp.exec($(this).css('border-top-width'))) + Number(regexp.exec($(this).css('border-bottom-width')));
			
			$(this).wrapInner('<span class="c"></span>').wrapInner('<span class="r"></span>').wrapInner('<span class="l"></span>');

			spanc = $(this).find('span.c');
			
			for(var i in border){
				spanc.css('border-top-'+border[i], $(this).css('border-top-'+border[i]));
				spanc.css('border-bottom-'+border[i], $(this).css('border-bottom-'+border[i]));
			}
			spanc.css('height', thisheight + 'px');
			
			$(this).attr('style', 'border:none;height:'+newheight+'px');

			if (parseInt(jQuery.browser.version) <= 6) {
				/*
				$(this).mouseover(function(){
					$(this).addClass('hover');
				}).mouseout(function(){
					$(this).removeClass('hover');
				});
				*/		
			}	    	
	    });
	}
}