	$.fn.ajaxBlocks = function() {
		
		return this.each(function(i, obj) {
			
			$this = $(this);
			
			var blocks = $(obj).find(".ajax-block");
 
			$.each(blocks, function(j, element) {

				$(element).html('').addClass('processing');
 
				 $.ajax({type: "GET",
                   url: $(element).data('url'),
                   dataType: "json",
                   async: true,
                   cache: true,
 
                   success: function(data) {
                	 //console.log(data);
	  				 if (data == null){
	  				   return false;
	  				 }
	  				 $(element).html(data);
	  			   },
 
				   error: function(jqXHR, textStatus, errorThrown) {
					 //console.log(textStatus + ': ' + errorThrown);
				   }
                 });
		   });
	 });
 };
