(function($) {
  
  function get_new_id(prefix){
    return prefix + parseInt(Math.random()*11, 10) + new Date().getTime();
  }
  
  function is_ipad(){
    return navigator.userAgent.match(/iPad/i) != null;
  }
  
  function dpl(element, opt){
    
    this.options = {
        isIpad:is_ipad(),
        player: {
            skin: '/sites/all/themes/dozer/swf/dplayer/tnplayer620.zip',
            flashplayer: '/sites/all/themes/dozer/swf/dplayer/player.swf',
            autostart: true
        }
    }

    if (opt != undefined){
      $.extend(this.options.player, opt);
    }    
    
    //console.log(this.options.player)
    
    var self = this;
    this.$element = $(element);
    
    //@todo: remove
    /*
    if (this.$element.data('op').ipad == true){
      this.options.isIpad = true;
    }    
    */
    
    this.$element.click(function(e){ self.open(e); });

    $.extend(this.options.player, this.$element.data('player'));
    
    
    if (this.$element.data('ads') != undefined){
      //this.options.player.plugins = this.$element.data('ads');
      this.options.player.plugins = {};
      this.options.player.plugins['/sites/all/themes/dozer/swf/dplayer/ova-jw.swf'] = this.$element.data('ads');
    }
    //console.log(this.options.player);
    
    if (this.options.player.modes != undefined){
      var l = this.options.player.modes.length;
      for (var i = 0; i < l; i++){
        if (this.options.player.modes[i].type == 'flash') {
          this.options.player.modes[i].src = this.options.player.flashplayer;
        }
      }
    }
        
  }
  
  dpl.prototype.open = function(e){
    var self = this;
    e.preventDefault();
    
    var options = $.extend({close:true,node:null,caption:null,css:null},this.$element.data('op')); 
    
    if (options.node == null){
      //this.$original = this.$element.parent();
      this.$original = this.$element;
    } else {
      this.$original = this.$element.closest(options.node);
    }
    
    this.$parent = this.$original.parent(); 

    var id = get_new_id('dp-');
    
    this.$player = $('<div id="' + id + '" class="dplayer-wrapper '+options.css+'"></div>');
    
    if ( (this.options.isIpad == false) && (options.close == true)) {
      this.$player.append('<div class="dp-close"></div>');
      if (options.caption != null){
        this.$player.append('<div class="dp-caption">' + options.caption +'</div>');
      }
      this.$player.find('.dp-close').click(function(e){ self.close(e); });
    }
    this.$player.append('<div id="'+ id +'_video"></div>');
    
    this.$original.after(this.$player);

    this.$player.height(this.options.player.height);
    this.$player.width(this.options.player.width);
    this.$original.addClass('hidden');
    
    jwplayer(id + '_video').setup(this.options.player);
    //console.log(this.options.player)
    
  };
  
  dpl.prototype.close = function(e){
    e.preventDefault();
    this.$original.removeClass('hidden');
    this.$player.remove();
  };
  
  $.fn.dplayer = function(opt){

    return this.each(function() {
      
      
      
      new dpl(this, opt);

      if ($(this).data('op').autostart == true){
        $(this).trigger('click');
      } else {
        if (is_ipad()) {
          if ($(this).data('op').force == true) {
            $(this).trigger('click');
          }
        }        
      }

    });
    
  };  

}(jQuery));  