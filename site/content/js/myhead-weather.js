/*
head.js(TN.BASE_PATH + 'js/libs/jquery-1.5.2.min.js',
        TN.BASE_PATH + 'js/libs/underscore-min.js',
        TN.BASE_PATH + 'js/libs/cplayer.js',
        TN.BASE_PATH + 'js/libs/tn.js',
        TN.BASE_PATH + 'js/libs/tn_fb.js',
        TN.BASE_PATH + 'js/plugins/jquery-eplanning.js',
        TN.BASE_PATH + 'js/plugins/jquery-lazyload.js',
        TN.BASE_PATH + 'js/plugins/jquery-coda-slider-2.0.js',
        TN.BASE_PATH + 'js/plugins/jquery-easing.1.3.js',
        TN.BASE_PATH + 'js/plugins/jcarousellite.js',
        TN.BASE_PATH + 'js/plugins/tn-history.js',
        TN.BASE_PATH + 'js/plugins/jquery.tools.min.js',
        TN.BASE_PATH + 'js/zinit.js', function() { TN_startup(); });
*/

$LAB.script(TN.BASE_PATH + 'js/libs/jquery-1.5.2.min.js').wait()
 	.script(TN.BASE_PATH + 'js/libs/tn_fb.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-lazyload.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-cluetip.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery.tools.min.js')
    .script(TN.BASE_PATH + 'js/libs/tn-reacciones.js')
    .script(TN.BASE_PATH + 'js/libs/tn-reac-cuadro.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-mustache.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery.tools.min.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-jclock.js').wait()
    .script(TN.BASE_PATH + 'js/weather-zinit.js');

if (head.browser.ie)  {
	//load d_belatedpng to fix png32 in ie6
	if (parseInt(head.browser.version) <= 6)   {
		//head.js(TN.BASE_PATH + 'js/libs/dd_belatedpng.js',TN.BASE_PATH + 'js/libs/pngfix.js');
		$LAB.script(TN.BASE_PATH + 'js/libs/dd_belatedpng.js',TN.BASE_PATH + 'js/libs/pngfix.js');
	}
}
