TN_startup = function() {
  if (TN.warming_up === true) { return; };
  TN.warming_up = true;
  $.each(TN.initfuncs, function(i,e) {
    TN[e].call();
  });
}

$LAB.script(TN.BASE_PATH + 'js/libs/jquery-1.5.2.min.js').wait()
    .script(TN.BASE_PATH + 'js/libs/tn.js')
    .script(TN.BASE_PATH + 'js/libs/tn_fb.js')
    .script(TN.BASE_PATH + 'js/plugins/jcarousellite-moviles.js')
    .script(TN.BASE_PATH + 'js/moviles.js').wait()
    .wait(function() { TN_startup(); });
