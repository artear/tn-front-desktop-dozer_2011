$(document).ready(function() {

	alert("ok");

	$("li.for, div.for-extend").hover(function() {
		$("#torneos-fixture").fadeIn("fast");
	}, function() {
		$("#torneos-fixture").fadeOut("fast");
	});
	
	$("#ver-asistidores").click(function(e) {
		e.preventDefault();
		$(".wGoleadores").hide();
		$(".wAsistidores").show();
	});
	
	$(".ver-goleadores").click(function(e) {
		e.preventDefault();
		$(".wAsistidores").hide();
		$(".wGoleadores").show();
	});
	
	$('#widget-fixture .options .date').click(function() {
		$('#teams').empty();
	});
	
	$('#torneos-fixture a').click(function() {
		$this = $(this);

		path = $this.data("path");
		fixture = $this.data("fixture");
		equipo = $this.data("equipo");

		// url:
		// fixture+"/"+path+"/"+equipo+".html",
		// 'http://172.16.48.220/tn/site/content/fixture/primeraa/equipos/colon.html'

		$.ajax({
			type : "GET",
			url : fixture+"/"+path+"/"+equipo+".html",
			dataType : "html",
			async : true,
			cache : false,
			crossDomain : true,
			success : function(result,i) {$('#teams').html(result);}
		});
	});
});