TN_startup = function() {
  if (TN.warming_up === true) { return; };
  TN.warming_up = true;
  $.each(TN.initfuncs, function(i,e) {
    TN[e].call();
  });
}

/*
head.js(TN.BASE_PATH + 'js/libs/jquery-1.5.2.min.js',
        TN.BASE_PATH + 'js/libs/underscore-min.js',
        TN.BASE_PATH + 'js/libs/cplayer.js',
        TN.BASE_PATH + 'js/libs/tn.js',
        TN.BASE_PATH + 'js/libs/tn_fb.js',
        TN.BASE_PATH + 'js/plugins/jquery-eplanning.js',
        TN.BASE_PATH + 'js/plugins/jquery-lazyload.js',
        TN.BASE_PATH + 'js/plugins/jquery-coda-slider-2.0.js',
        TN.BASE_PATH + 'js/plugins/jquery-easing.1.3.js',
        TN.BASE_PATH + 'js/plugins/jcarousellite.js',
        TN.BASE_PATH + 'js/plugins/tn-history.js',
        TN.BASE_PATH + 'js/plugins/jquery.tools.min.js',
        TN.BASE_PATH + 'js/zinit.js', function() { TN_startup(); });
*/


$LAB.script(TN.BASE_PATH + 'js/libs/jquery-1.5.2.min.js').wait()
    .script(TN.BASE_PATH + 'js/libs/tn.js')
    .script(TN.BASE_PATH + 'js/libs/tn_fb.js')
    .script(TN.BASE_PATH + 'js/plugins/jcarousellite-moviles.js')
    .script(TN.BASE_PATH + 'js/moviles.js').wait()
    .wait(function() { TN_startup(); });