TN.init_headerad = function(visible) {

  if (visible) {
    setTimeout("TN.init_headerad(false)", 10000);
  } else {
	  $('#headerad .text').css({color:'#ffffff'});
  }
  $('#headerad .bg').fadeToggle('slow');


	/*
  if (visible) {
    $('#headerad .text').animate({backgroundColor: '#FFFFAA', color: '#444444'});
    setTimeout("TN.init_headerad(false)", 10000);
  } else {
    $('#headerad .text').animate({backgroundColor: '#BE0000', color: '#ffffff'});
  }
  */
}


function slideSwitch() {
    var $active = $('.slideshow a.active');

    if ($active.length === 0) $active = $('.slideshow a:last');

    var $next = $active.next().length ? $active.next() : $('.slideshow a:first');

    $active.addClass('last-active');

    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
        });
}


function setupTopGrid() {
    var SHOW_GRID_CLASS = 'show-grid';
    var ANIM_SPEED1 = 579;
    var ANIM_SPEED2 = 21;
    var $grid = $('#grid div.content');
    var $body = $('body');
    var $tgrid = $('#toggle-grid');

    var initHeight = $grid.height();
    var initPaddingTop = $grid.css('paddingTop');
    var initPaddingBottom = $grid.css('paddingBottom');

    // Caso: el grid carga cerrado
    if (! $body.hasClass(SHOW_GRID_CLASS)) {
        $grid.css({height: 0, paddingTop: 0});
    }

    $tgrid.mousedown(function() {
        $grid.stop(false, false);

        if ($body.hasClass(SHOW_GRID_CLASS)) {
            $body.removeClass(SHOW_GRID_CLASS);
            $grid
                .show()
                .animate({paddingBottom: 0}, ANIM_SPEED2, 'linear')
                /* Aquí no uso la cola de animación de jQuery para evitar
                el cambio súbito del paddin(timegTop (se ve como si el contenido
                chocara) al parar la animación.
                */
                .animate({height: 0}, ANIM_SPEED1, function() {
                    $grid.animate({paddingTop: 0}, ANIM_SPEED2, 'linear',
                        function() {
                            $tgrid.removeClass('down');
                            // Corrige bug IE6: El paddingTop queda hasta que la sección
                            // se redibuja.
                            $grid.hide();
                        }
                    );
                });
        }
        else {
            $body.addClass(SHOW_GRID_CLASS);
            $grid
                .show()
                .animate({paddingTop: initPaddingTop}, ANIM_SPEED2, 'linear')
                .animate({height: initHeight}, ANIM_SPEED1, function() {
                    $tgrid.addClass('down');
                    // Para activar el lazy-loading de las imágenes
                    $(window).trigger('scroll');
                })
                .animate({paddingBottom: initPaddingBottom}, ANIM_SPEED2, 'linear');
        }
        return false;
    });
}

function setupPlaceholders() {
    $('input.placeholder')
        .focus(function(e) {
            var $input = $(this);
            $input.removeClass('placeholder');
            if ($input.val() == $input.attr('data-placeholder')) {
                $input.val('');
            }
        })
        .blur(function(e) {
            var $input = $(this);
            var val = $.trim($input.val());
            var placeholder = $input.attr('data-placeholder');
            if ((val === '') || (val === placeholder)) {
                $input.addClass('placeholder').val(placeholder);
            }
            else {
                $input.removeClass('placeholder');
            }
        })
        .trigger('blur');
}

function showoverlay(a, parent, overlay){

  if (overlay == undefined){
	  overlay = '.overlay'
  }
	  
  $('body').append('<div id="overlay"></div>');

  var $overlay = $('#overlay');
  var $overlayContent = $(a).closest(parent).find(overlay);
  $overlayContent.clone().prependTo($overlay);

  $overlay.find('.overlay').removeClass('hidden');
  $overlay.find('#id').val($(a).attr('data-id'));

  $overlay.overlay({
    close:'div.close',
    target:'#overlay',
    speed:'slow',
    top:'50%',
    mask: {
      color: '#000',
      loadSpeed: 200,
      opacity: 0.6
    },
    onBeforeLoad: function(){
      $('#overlay div.content').height( $('#overlay div.active').height() );
    },
    onClose: function() {
      $('#overlay').remove();
    },
    load: true
  });

  $overlay.css('margin-top', '-' + $overlay.outerHeight()/2 + 'px');
}

function first_load() {

  $('#sub-menu .close a').click(function (){
     $('#navigation .more a').click();
     return false;
  })

  /*
  $('.not-logged-in .login-box .content').tooltip({
    effect: 'slide',
    relative: 'true'
  }).dynamic({ bottom: { direction: 'down', bounce: true } });
  */

  $('#navigation .more a').click(function(e) {

    var ANIM_SPEED1 = 500;
    var ANIM_SPEED2 = ANIM_SPEED1 / 2;

    var $container = $('#sub-menu');
    var $menu = $('#sub-menu #menu-' + $(this).attr('data-menu'));
    var $li = $(this).closest('li.more');

    if ($menu.hasClass('active') === true) {
      $li.removeClass('more-expanded');
        $container.slideUp(ANIM_SPEED2, function() {
          $container.removeClass('show-submenu');
          $menu.css({display: 'none'}).removeClass('active');

          return false;
        });
      return false;
    }

    if ($container.hasClass('show-submenu')) {
      var $this = $(this);
      $container.slideUp(ANIM_SPEED2, function() {
        $container.removeClass('show-submenu');
        $('#sub-menu .menu').css({display: 'none'}).removeClass('active');
        $this.trigger('click');
        return false;
      });
      return false;
    }

    if ($menu.hasClass('active') === false) {
      //muestra el menu en si
      $menu.css({display: 'block'}).addClass('active');

      //expande el contenedor
      $li.addClass('more-expanded');
      $container.slideDown(ANIM_SPEED1, function() {
        $container.addClass('show-submenu');
        return false;
      });
      return false;
     }
    return false;

  });

  $('.fbconnect-tn').live('click', function(e) {
    FB.login(function(response) {
      if (response.authResponse) {
        // var accessToken = response.authResponse.accessToken;
        $("#fbconnect-autoconnect-form").submit();
      } else {
        //user cancelled login or did not grant authorization
      }
    }, {
      scope : 'read_stream,publish_stream,email'
    });
    e.preventDefault();
  });
}


$(document).ready(function() {

  $("#fixture table .detalle a").click(function(e){
    e.preventDefault();
    var $thisHref = $(this).attr('href');
    window.open($thisHref, "_blank", "resizable=no, toolbar=0, status=0, menubar=0,location=0, scrollbars=1, width=1100, height="+screen.height+", top=0, left=120");
  });
  
  $('#fixture table').hide();
  $('#fixture table[data-current="true"]').show();
  $('.options-extend li[data-current="true"]').addClass('select');

  $(".options-extend ul li").click(function(){
    $(".options-extend ul li").removeClass('select');
    $(this).addClass('select');
    var $dataSelection = $(this).data('selection');

    $("#fixture table").hide();
    $("#fixture table" + "#" + $dataSelection ).show();
    
  });

  $("#torneos-fixture li").click(function(){
    var $dataSelection = $(this).data('equipo');
    $("#fixture table").hide();
    $("#fixture table" + ".resequipo-" + $dataSelection).show();
  });

  /*$('#fixture').jCarouselLite({
      btnNext: '#fixture .next',
      btnPrev: '#fixture .prev',
      visible: 5,
      wrapper: 'table',
      element: 'tr',
      vertical:true,
      circular: false,
      speed: 1000,
      start: 0,
      scroll: 1
    });*/

    $("li.for, div.for-extend").hover(function() {
    $("#torneos-fixture").fadeIn("fast");
  }, function() {
    $("#torneos-fixture").fadeOut("fast");
  });
  
  $("#ver-asistidores").click(function(e) {
    e.preventDefault();
    $(".wGoleadores").hide();
    $(".wAsistidores").show();
  });
  
  $(".ver-goleadores").click(function(e) {
    e.preventDefault();
    $(".wAsistidores").hide();
    $(".wGoleadores").show();
  });
  
  $('#widget-fixture .options .date').click(function() {
    $('#teams').empty();
  });
  
  $('#torneos-fixture a').click(function() {
    $this = $(this);

    path = $this.data("path");
    fixture = $this.data("fixture");
    equipo = $this.data("equipo");

    // url:
    // fixture+"/"+path+"/"+equipo+".html",
    // 'http://172.16.48.220/tn/site/content/fixture/primeraa/equipos/colon.html'

    $.ajax({
      type : "GET",
      url : fixture+"/"+path+"/"+equipo+".html",
      dataType : "html",
      async : true,
      cache : false,
      crossDomain : true,
      success : function(result,i) {$('#teams').html(result);}
    });
  });

	  var json = {"node":[{"text":"Probá TN Flip!","url":"http://tn.com.ar/flip","class":"ad-flip"},{"text":"¿Ya viste La Viola?","url":"http://tn.com.ar/musica","class":"ad-viola"},{"text":"Llevá TN con Vos","url":"http://m.tn.com.ar","class":"ad-tn-mobile"},{"text":"¿Ya viste TN HD?","url":"http://tn.com.ar/envivo/24hs/HD","class":"ad-hd"},{"text":"Volvé a ver los Programas","url":"http://tn.com.ar/programas","class":"ad-programas"},{"text":"Lo más leído ahora","url":"http://tn.com.ar/mas-leidas","class":"ad-more-readed"},{"text":"Los temas del momento en TN Trends","url":"http://tn.com.ar/trends","class":"ad-trends"},{"text":"Todo el deporte está en Toda Pasión","url":"http://tn.com.ar/deportes","class":"ad-tp"},{"text":"Sorteos de Loterías y Quinelas","url":"http://mam.tn.com.ar/loterias","class":"ad-quinela"},{"text":"La farándula estalla en TN Famosos","url":"http://tn.com.ar/show","class":"ad-show"}]};

	  var item = Math.floor(Math.random()*json['node'].length);
	  item = json['node'][item];
	  $('#headerad').addClass(item['class']);
	  $('#headerad').append('<a class="text" href="' + item['url'] + '" title="' + item['text'] + '" target="_blank">' + item['text'] + '</a>')



  setupPlaceholders();
  setupTopGrid();

  TN.setup_tn_facebook();
  TN.load_facebook_js();

  if ($('body').hasClass('site-tn')){
	  $('.site-tn #header .hd').tooltip({
			  tipClass: 'tooltip-hd',
			  offset: [-5,10],
			  effect:'fade',
			  position: 'center right'
			});
  }

  //$('iframe.facebook-share-button').lazyload({appear: TN.load_social_iframe});
  //$('iframe.twitter-share-button').lazyload({appear: TN.load_social_iframe});
  $('div.twitter-share-button').lazyload({appear: TN.load_twitter_button});
  $('iframe.async-iframe').lazyload({appear: TN.load_social_iframe});
  $('div.g-plusone').lazyload({appear: TN.load_glg_button});
  $('.fb-button').lazyload({appear: TN.load_fb_asyn});
  $('img.lzl').lazyload({'effect': 'fadeIn'});

  if ($.browser.ie && $.browser.version === 6) {
    $('.ie6 span.featured').each(function (index){
      $(this).height( $(this).closest('.content').height() );
    });
  };

  $('#viewmore-ranking').click(function(ev) {
    $(ev.target).attr('href', $('#ranking li.selected').find('.xtrig').attr('data-path'));
  });

  TN.init_analytics_social();

  //window.setTimeout( "TN.delayed_google_button()", 1000 );

  $('body').addClass('js-finished');


  TN.init_headerad(true);
  
  TN.ajaxBlocks();
  
  $('input.reset').click(function(){history.back();});
});

/*
TN.delayed_google_button = function() {
  window.___gcfg = {
    lang: 'es-419',
    parsetags: 'explicit'
  };
  console.log('1');
  var script_tag = $('<script><\/script>');
  console.log('2');
  script_tag.attr('type', 'text/javascript');
  console.log('3');
  script_tag.bind('load', function(e)
  {
  console.log('4');
    $('div.g-plusone').lazyload({appear: TN.load_glg_button});
  });
  console.log('5');
  //script_tag.append('{"lang": "es-419", "parsetags": "explicit"}');
  console.log('6');
  script_tag.attr('src', 'https://apis.google.com/js/plusone.js');
  console.log('7');
  $('head')[0].appendChild(script_tag[0]);
}
*/

