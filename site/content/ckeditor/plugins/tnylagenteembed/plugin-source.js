/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

/**
 * @file Plugin for inserting Drupal embeded media
 */
( function() {
  CKEDITOR.plugins.add( 'tnylagenteembed',
  {
    requires : [ 'fakeobjects', 'htmlwriter' ],
    init: function( editor )
    {
    	var w = editor.config.width - 80;
      editor.addCss(
        'img.cke_tnylagenteembed' +
        '{' +
          'background-image: url(' + CKEDITOR.getUrl( this.path + 'images/placeholder.png' ) + ');' +
          'background-position: center center;' +
          'background-repeat: no-repeat;' +
          'border: 1px solid #a9a9a9;' +
          'width: ' + w + 'px;' +
          'margin: 0 auto;' +
          'height: 200px;' +
          'display:block;' +
        '}' +
        'img.cke_tnylagenteembed:hover' +
        '{' +
        	'opacity:.85;' +
        '}'
        );
      var me = this;
      CKEDITOR.dialog.add( 'TNMediaEmbedDialog', function( editor ) {
        return {
          title : 'Agregar videos de TN y La Gente',
          minWidth : 400,
          minHeight : 100,
          contents : [
            {
              id : 'mediaTab',
              label : 'Agregar url del video',
              title : 'Agregar url del video',
              elements :
              [
                {
                  id : 'embed',
                  type : 'text',
                  label : 'Pegar código aqui',
                  validate : CKEDITOR.dialog.validate.notEmpty('Debe indicar la URL del video.')
                },{
                  type: 'html',
                  html: 'Ejemplo: <ul><li>http://www.youtube.com/watch?v=324UsTQsHAM</li></ul>'
                }
              ]
            }
          ],
          onOk : function() {
            var editor = this.getParentEditor();
            var content = this.getValueOf( 'mediaTab', 'embed' );
            if ( content.length>0 ) {
              var realElement = CKEDITOR.dom.element.createFromHtml('<div class="media_embed"></div>');
              content = '[[video:' + content + ']]';
              realElement.setHtml(content);
              var fakeElement = editor.createFakeElement( realElement , 'cke_tnylagenteembed', 'div', true);
              var matches = content.match(/width=(["']?)(\d+)\1/i);
              if (matches && matches.length == 3) {
                fakeElement.setStyle('width', cssifyLength(matches[2]));
              }
              matches = content.match(/height=([\"\']?)(\d+)\1/i);
              if (matches && matches.length == 3) {
                fakeElement.setStyle('height', cssifyLength(matches[2]));
              }
              editor.insertElement(fakeElement);
            }
          }
        };
      });

      editor.addCommand( 'TNMediaEmbed', new CKEDITOR.dialogCommand( 'TNMediaEmbedDialog' ) );

      editor.ui.addButton( 'TNMediaEmbed',
      {
        label: 'Video de TN y La Gente',
        command: 'TNMediaEmbed',
        icon: this.path + 'images/icon.png'
      } );
    },
    afterInit : function( editor )
    {
      var dataProcessor = editor.dataProcessor,
        dataFilter = dataProcessor && dataProcessor.dataFilter,
        htmlFilter = dataProcessor && dataProcessor.htmlFilter;

      if ( htmlFilter )
      {
        htmlFilter.addRules({
          elements :
          {
            'div' : function ( element ) {
              if( element.attributes['class'] == 'media_embed' ) {
                for (var x in element.children) {
                  if (typeof(element.children[x].attributes) != 'undefined') {
                    if (typeof(element.children[x].attributes.width) != undefined) {
                      element.children[x].attributes.width = element.attributes.width;
                    }
                    if (typeof(element.children[x].attributes.height) != undefined) {
                      element.children[x].attributes.height = element.attributes.height;
                    }
                  }
                }
              }
            }
          }
        });
      }
      if ( dataFilter )
      {
        dataFilter.addRules(
          {
            elements :
            {
              'div' : function( element )
              {
                var attributes = element.attributes,
                  classId = attributes.classid && String( attributes.classid ).toLowerCase();
                  
                if (element.attributes[ 'class' ] == 'media_embed') {
                  var fakeElement = editor.createFakeParserElement(element, 'cke_tnylagenteembed', 'div', true);
                  var fakeStyle = fakeElement.attributes.style || '';
                  if (typeof(element.children[0].attributes) != 'undefined') { 
                    var height = element.children[0].attributes.height,
                      width = element.children[0].attributes.width;
                  }
                  if ( typeof width != 'undefined' )
                    fakeStyle = fakeElement.attributes.style = fakeStyle + 'width:' + cssifyLength( width ) + ';';
              
                  if ( typeof height != 'undefined' )
                    fakeStyle = fakeElement.attributes.style = fakeStyle + 'height:' + cssifyLength( height ) + ';';
                 
                  return fakeElement;  
                }
                return element;
              }
            }
          },
          5);
      }
    }
  } );
  var numberRegex = /^\d+(?:\.\d+)?$/;
  function cssifyLength( length )
  {
    if ( numberRegex.test( length ) )
      return length + 'px';
    return length;
  }
} )();
