echo 'Init vars...'

js="output/js/"
jslibs="${js}libs/"
jsplugins="${js}plugins/"
tmp="tmp/"
tmpjslibs="${tmp}js/libs/"
tmpjsplugins="${tmp}js/plugins/"
tmpjsclima="${tmp}js/clima/"

echo 'Compresing js...'

mkdir -p tmp/js/libs
mkdir -p tmp/js/plugins

echo 'twitter-widget-min'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjslibs}10_twitter-widget-min.js ${jslibs}twitter-widget-min.js
echo 'tn_fb.js'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjslibs}20_tn_fb-min.js ${jslibs}tn_fb.js
echo 'tn.js'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjslibs}30_tn-min.js ${jslibs}tn.js
echo 'cplayer'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjslibs}40_cplayer-min.js ${jslibs}cplayer.js
echo 'cplayer'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjslibs}45_dplayer-min.js ${jsplugins}jquery-dplayer.js
echo 'custom-drupal'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjslibs}50_custom-drupal.js ${jslibs}custom-drupal.js
echo 'fbconnect'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjslibs}60_fbconnect.js ${jslibs}fbconnect.js
echo 'tw_carousel'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjslibs}70_tw_carousel.js ${jslibs}tw_carousel.js
echo 'ie-pinned'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjslibs}80_ie-pinned.js ${jslibs}ie-pinned.js

cp ${jslibs}jwplayer.js ${tmpjslibs}35_jwplayer-min.js
#cp ${jslibs}fbconnect.js ${tmpjslibs}50_fbconnect.js

echo 'jquery-easing'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsplugins}10_jquery-easing-1-3-min.js ${jsplugins}jquery-easing.1.3.js
echo 'jquery-coda-slider'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsplugins}15_jquery-coda-slider-2.0-min.js ${jsplugins}jquery-coda-slider-2.0.js
echo 'jquery-eplanning'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsplugins}20_jquery-eplanning-min.js ${jsplugins}jquery-eplanning.js
echo 'ajaxblocks'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsplugins}35_ajaxblocks.js ${jsplugins}ajaxblocks.js
echo 'jquery-lazyload'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsplugins}30_jquery-lazyload-min.js ${jsplugins}jquery-lazyload.js
echo 'jquery-cookie'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsplugins}35_jquery-cookie-min.js ${jsplugins}jquery-cookie.js
echo 'jquery-keycounter'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsplugins}38_jquery-keycounter-min.js ${jsplugins}jquery-keycounter.js
echo 'jquery-fancybox'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsplugins}40_jquery-facybox.js ${jsplugins}jquery_fancybox.js
echo 'jquery-jgfeed'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsplugins}45_jquery_jgfeed_min.js ${jsplugins}jquery_jgfeed_min.js

cp ${jsplugins}jquery-isotope-min.js ${tmpjsplugins}39_jquery-isotope-min.js
cp ${jsplugins}jcarousellite-min.js ${tmpjsplugins}50_jcarousellite-min.js
cp ${jsplugins}tn-history.js ${tmpjsplugins}60_tn-history.js
cp ${jsplugins}jquery.tools.min.js ${tmpjsplugins}70_jquery.tools.min.js
cp ${jsplugins}jquery-animate-colors-min.js ${tmpjsplugins}80_jquery-animate-colors-min.js

java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmp}js/zinit-min.js  ${js}zinit.js

echo 'compresing wheater'

mkdir -p tmp/js/clima
cp ${tmpjslibs}20_tn_fb-min.js ${tmpjsclima}10_tn_fb-min.js
cp ${tmpjslibs}10_twitter-widget-min.js ${tmpjsclima}20_twitter-widget-min.js
cp ${tmpjsplugins}30_jquery-lazyload-min.js ${tmpjsclima}30_jquery-lazyload-min.js
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsclima}40_jquery-cluetip.js ${jsplugins}jquery-cluetip.js
cp ${jsplugins}jquery.tools.min.js ${tmpjsclima}50_jquery.tools.min.js
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsclima}60_tn-reacciones.js ${jslibs}tn-reacciones.js
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsclima}70_tn-reac-cuadro.js ${jslibs}tn-reac-cuadro.js
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsclima}80_jquery-mustache.js ${jsplugins}jquery-mustache.js
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsclima}90_jquery-ui-min.js ${jsplugins}jquery-ui-min.js
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsclima}100_jquery-jclock.js ${jsplugins}jquery-jclock.js
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmpjsclima}110_weather-zinit.js  ${js}weather-zinit.js

echo 'making my magic for the publish'
mkdir ${tmp}pl

echo 'compresing drupal shit'
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmp}pl/10_drupal.js ${jsplugins}drupal.js
#java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmp}pl/15_tabledrag.js ${jsplugins}tabledrag.js
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmp}pl/20_autocomplete.js ${jsplugins}autocomplete.js
#cp ${jsplugins}jquery-form-min.js ${tmp}pl/30_jquery-form-min.js
#java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmp}pl/40_ahah.js ${jsplugins}ahah.js

echo 'compresing the great pluploader'
#java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmp}pl/50_plupload.js ${jslibs}plupload-min.js
#java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmp}pl/60_plupload-flash.js ${jslibs}plupload-flash-min.js
cp ${jslibs}plupload-min.js ${tmp}pl/50_plupload-min.js
cp ${jslibs}plupload-flash-min.js ${tmp}pl/60_plupload-flash-min.js
cp ${jsplugins}jquery-plupload-queue-min.js ${tmp}pl/70_jquery-plupload-queue-min.js
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o ${tmp}pl/80_pluploader-config.js ${jslibs}plupload-config.js

mkdir -p ${js}prod

cat ${tmpjsplugins}* > ${js}prod/tn.js
cat ${tmpjslibs}* >> ${js}prod/tn.js
cat ${tmp}js/*.js >> ${js}prod/tn.js
cat ${tmp}pl/*.js > ${js}prod/publish-min.js
cat ${tmpjsclima}*.js > ${js}prod/weather-min.js

rm -rf tmp

java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o output/js/tpiframes-min.js output/js/tpiframes.js

echo 'Compress finished \o/, we saved the internet'
