mkdir -p tmp/libs
mkdir -p tmp/plugins
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/libs/10_twitter-widget-min.js libs/twitter-widget-min.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/libs/20_tn_fb-min.js libs/tn_fb.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/libs/30_tn-min.js libs/tn.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/libs/40_cplayer-min.js libs/cplayer.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/libs/50_custom-drupal.js libs/custom-drupal.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/libs/60_fbconnect.js libs/fbconnect.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/libs/70_tw_carousel.js libs/tw_carousel.js

cp libs/jwplayer.js tmp/libs/35_jwplayer-min.js
#cp libs/fbconnect.js tmp/libs/50_fbconnect.js

java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/plugins/10_jquery-easing-1-3-min.js plugins/jquery-easing.1.3.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/plugins/15_jquery-coda-slider-2.0-min.js plugins/jquery-coda-slider-2.0.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/plugins/20_jquery-eplanning-min.js plugins/jquery-eplanning.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/plugins/30_jquery-lazyload-min.js plugins/jquery-lazyload.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/plugins/35_jquery-cookie-min.js plugins/jquery-cookie.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/plugins/38_jquery-keycounter-min.js plugins/jquery-keycounter.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/plugins/40_jquery-facybox.js plugins/jquery_fancybox.js
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/plugins/45_jquery_jgfeed_min.js plugins/jquery_jgfeed_min.js

cp plugins/jcarousellite-min.js tmp/plugins/50_jcarousellite-min.js
cp plugins/tn-history.js tmp/plugins/60_tn-history.js
cp plugins/jquery.tools.min.js tmp/plugins/70_jquery.tools.min.js
cp plugins/jquery-animate-colors-min.js tmp/plugins/80_jquery-animate-colors-min.js

java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o tmp/zinit-min.js  zinit.js

cat tmp/plugins/* > prod/tn.js
cat tmp/libs/* >> prod/tn.js
cat tmp/*.js >> prod/tn.js

rm -rf tmp
