echo 'Init vars...'

js="output/js/"
jslibs="${js}libs/"
jsplugins="${js}plugins/"
tmp="tmp/"
tmpjs="tmp/js/"
tmpjslibs="${tmp}js/libs/"
tmpjsplugins="${tmp}js/plugins/"

mkdir -p tmp/css

echo 'Compresing css...'
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type css --line-break 200 -o tmp/css/00_reset.css  content/css/reset.css
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type css --line-break 200 -o tmp/css/10_fonts.css  content/css/fonts.css
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type css --line-break 200 -o tmp/css/20_style.css  content/css/style.css

java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type css --line-break 200 -o tmp/css/30_twitter-widget.css  content/css/twitter-widget.css
java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type css --line-break 200 -o tmp/css/40_jquery-ui-custom.css  content/css/blitzer/jquery-ui-custom.css

java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type css --line-break 200 -o content/css/ie-min.css  content/css/ie.css

cat tmp/css/* > content/css/weather-min.css

rm -rf tmp/css

echo 'Compresing js...'

mkdir -p tmp/js
#mkdir -p tmp/js/libs
#mkdir -p tmp/js/plugins

java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o ${tmpjs}10_tn_fb-min.js ${jslibs}tn_fb.js

cp ${jslibs}twitter-widget-min.js ${tmpjs}15_twitter-widget-min.js

java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o ${tmpjs}20_jquery-lazyload-min.js ${jsplugins}jquery-lazyload.js

java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o ${tmpjs}30_jquery-cluetip-min.js ${jsplugins}jquery-cluetip.js

cp ${jsplugins}jquery.tools.min.js ${tmpjs}40_jquery.tools.min.js

java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o ${tmpjs}50_tn-reacciones-min.js ${jslibs}tn-reacciones.js

java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o ${tmpjs}60_tn-reac-cuadro-min.js ${jslibs}tn-reac-cuadro.js

java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o ${tmpjs}70_jquery-mustache-min.js ${jsplugins}jquery-mustache.js

cp ${jsplugins}jquery-ui-min.js ${tmpjs}80_jquery-ui-min.js

java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o ${tmpjs}90_jquery-jclock-min.js ${jsplugins}jquery-jclock.js


java -jar /opt/utils/yuicompressor-2.4.6/build/yuicompressor-2.4.6.jar --type js --line-break 200 -o ${tmpjs}/999_weather-zinit-min.js  ${js}weather-zinit.js

mkdir -p ${js}prod

cat tmp/js/* > ${js}prod/weather.js

rm -rf tmp/*

echo 'Compress finished \o/, we saved the internet'
