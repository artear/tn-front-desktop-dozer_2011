function setupPlaceholders() {
    $('input.placeholder')
        .focus(function(e) {
            var $input = $(this);
            $input.removeClass('placeholder');
            if ($input.val() == $input.attr('data-placeholder')) {
                $input.val('');
            }
        })
        .blur(function(e) {
            var $input = $(this);
            var val = $.trim($input.val());
            var placeholder = $input.attr('data-placeholder');
            if ((val === '') || (val === placeholder)) {
                $input.addClass('placeholder').val(placeholder);
            }
            else {
                $input.removeClass('placeholder');
            }
        })
        .trigger('blur');
}

function twsearch(id, search){
    var twt_search = new TWTR.Widget({
    	id: id,
        version : 2,
        type : 'search',
        search : search,
        interval : 4000,
        width : 593,
        height : 1000,
        theme : {
          shell : {
            background : '#ffffff',
            color : '#444444'
          },
          tweets : {
            background : '#ffffff',
            color : '#444444'
          }
        },
        features : {
          scrollbar : false,
          loop : true,
          live : true,
          hashtags : true,
          timestamp : true,
          avatars : true,
          toptweets : true,
          behavior : 'default'
        }
      });
    	twt_search.render();
		  return twt_search;
}

$(document).ready(function() {
  setupPlaceholders();

  $('img.lzl').lazyload({'effect': 'fadeIn'});


  $('.jclock').jclock({format: '%H:%M'});

  $('.reac').each(function(i,e) {

	  $(e).html( $.mustache(rcbox, {box_name: 'clima'+i, box_title: 'Ideal para...'} ) );
	  var temp_desc = $('#max'+i).text() + ' ~ ' + $('#min'+i).text();
	  var date = '';
	  var $section = $(this).parents('section');
	  
	  if ($section.attr('id') == 'today'){
		  var d = new Date();
		  var date = d.getDate() + '/' + (d.getMonth() + 1);
	  } else {
		  var t = $(this).parents('.day').find('time').attr('datetime');
		  t = t.split('.');
		  date = t[0] + '/' + t[1];
	  }

	  //console.log('Datos del clima de TN.com.ar para el ' + date);
	  
	  var cc = TN.reacciones({
	    dom: '#rcbox_clima'+i,
	    data: opc,
	    fb_desc: { name: 'Datos del clima de TN.com.ar para el ' + date,
	               description: temp_desc,
	               href: document.URL,
	               media: { type: 'image',
	            	   		src: 'http://cdn.tn.com.ar/sites/all/themes/dozer/images/logo-tn.png',
	            	   		href: document.URL }
	              },
	    options: fix
	  });
	  cc.setup();
	  cc.builDom();
	});

  $('a.ideal-for').cluetip({
	  local: true,
	  showTitle: false,
	  sticky: true,
	  cursor: 'pointer',
	  closeText: '',
	  fx: { open: 'fadeIn', openSpeed: 'fast' },
	  cluetipClass: 'rounded'
	});

  $('.hd').tooltip({tipClass: 'tooltip-hd',offset: [-5,10],effect:'fade',position: 'center right'});

  TN.clima_auto_cache = {};
  TN.clima_auto_lastXhr = "";
  TN.clima_auto_cache2 = {};
  TN.clima_auto_lastXhr2 = "";

  $('#edit-provincia').autocomplete( {
    minLength: 2,
    source: function( request, response ) {
      var term = request.term;
      if ( term in TN.clima_auto_cache ) {
        response( TN.clima_auto_cache[ term ] );
        return;
      }

      TN.clima_auto_lastXhr = $.getJSON( "/clima/autocomplete.json", request, function( data, status, xhr ) {
        TN.clima_auto_cache[ term ] = data;
        if ( xhr === TN.clima_auto_lastXhr ) {
          response( data );
        }
      });
    },
    select: function( event, ui ) {
      if (ui.item) {
        $('#form-provincia .input-state').show();
        $('#form-provincia').data('provincia', ui.item.id);
        console.log('provincia', ui.item.id, ui.item);
      } else {
        $('#edit-provincia').value('');
        $('#form-provincia .input-state').hide();
      }
    }
  });

  $('#edit-ciudad').autocomplete( {
    minLength: 2,
    source: function( request, response ) {
      var term = request.term;
      if ( term in TN.clima_auto_cache2 ) {
        response( TN.clima_auto_cache2[ term ] );
        return;
      }

      TN.clima_auto_lastXhr2 = $.getJSON( "/clima/autocomplete.json?prov="+$('#form-provincia').data('provincia'), request, function( data, status, xhr ) {
        TN.clima_auto_cache2[ term ] = data;
        if ( xhr === TN.clima_auto_lastXhr2 ) {
          response( data );
        }
      });
    },
    select: function( event, ui ) {
      if (ui.item) {
        $('#form-ciudad .input-state').show();
        $('#form-ciudad').data('ciudad', ui.item.id);
        console.log('ciudad', ui.item.id, ui.item);
      } else {
        $('#edit-provincia').value('');
        $('#form-provincia .input-state').hide();
      }
    }
  });
  
  tn_src = twsearch('twtsearch', $('#twtsearch').data('search'));
  setTimeout('tn_src.start()', 3500);  

  window.fbAsyncInit = function() {
	  FB.init({ appId: '346699902992', 
		    status: true, 
		    cookie: true,
		    xfbml: true,
		    oauth: true});	
  }
  
  (function() {
	  var e = document.createElement('script');
	  e.async = true;
	  e.src = document.location.protocol + '//connect.facebook.net/es_LA/all.js';
	  document.getElementById('fb-root').appendChild(e);
	}());  
  

  $('body').addClass('js-finished');

});
