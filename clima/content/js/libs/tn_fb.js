/**************************************************************************
  Rutinas de ayuda para Facebook

  Rutina de ayuda para conectarse a FB y publicar en el wall del usuario.
  el ejemplo mas basico es:

  TN.FB.publish('mensaje de prueba');

  Si el usuario no esta logueado, se va a realizar el login y se van a
  solicitar los permisos de ser necesario.

  Un ejemplo mas completo puede ser:

    TN.FB.publish(
        { 'message': 'mensaje a compatir',
          'attachment': { 'name': 'titulo de la nota',
                          'description': 'bajada de la nota',
                          'href': 'path/a/a/nota',
                          'media': [{ 'type': 'image',
                                      'src': 'path/a/la/imagen',
                                      'href': 'link/a/la/nota'}]
                        }
        });

message, attachment, action_links, target, callback

 *************************************************************************/

TN.FB = function(opts) {
  var that = {};
  var data = opts;

  var loginFB = function(callback) {
	  FB.login(function(response) {
		  if (response.authResponse) {
		      if (response.status === 'connected') {
		          callback(response)
		      }		    
		  }
		}, {scope: 'email'});   
  };
  that.loginFB = loginFB;
  
  var publish = function() {
    //this.data = {message: message, attachment: attachment, action_links: action_links, target: target, callback: callback};

	FB.getLoginStatus( function(response) {
	    if ( response.status == 'connected' ) {
	      streamPublish();
	    } else {
	      loginFB( streamPublish );
	    }
	  });	  
	  
  };	
  that.publish = publish;

  var streamPublish = function() {
	  console.log(data);
	  /*
    FB.ui({ method: 'feed',
            message: data.message,
            attachment: data.attachment,
            action_links: data.action_links,
            target_id: data.target
          }, function(response) {
              if (data.callback) data.calback(response);
            }
    );
    */

		var obj = {
		  method: 'feed',
		  message: data.message,
		  link: data.attachment.href,
		  picture: data.attachment.media.src,
		  name: data.attachment.name,
		  caption: data.attachment.description,
		  description: data.message
		};

		function callback(response) {
			if (data.callback) data.calback(response);
		}
		
		FB.ui(obj, callback);	  

  };
  that.streamPublish = streamPublish;

  return that;
};
