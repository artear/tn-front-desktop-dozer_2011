function slideSwitch() {
    var $active = $('.slideshow img.active');

    if ($active.length === 0) $active = $('.slideshow img:last');

    var $next = $active.next().length ? $active.next() : $('.slideshow img:first');

    $active.addClass('last-active');

    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
        });
}


function setupTopGrid() {
    var SHOW_GRID_CLASS = 'show-grid';
    var ANIM_SPEED1 = 579;
    var ANIM_SPEED2 = 21;
    var $grid = $('#grid div.content');
    var $body = $('body');
    var $tgrid = $('#toggle-grid');

    var initHeight = $grid.height();
    var initPaddingTop = $grid.css('paddingTop');
    var initPaddingBottom = $grid.css('paddingBottom');

    // Caso: el grid carga cerrado
    if (! $body.hasClass(SHOW_GRID_CLASS)) {
        $grid.css({height: 0, paddingTop: 0});
    }

    $tgrid.mousedown(function() {
        $grid.stop(false, false);

        if ($body.hasClass(SHOW_GRID_CLASS)) {
            $body.removeClass(SHOW_GRID_CLASS);
            $grid
                .show()
                .animate({paddingBottom: 0}, ANIM_SPEED2, 'linear')
                /* Aquí no uso la cola de animación de jQuery para evitar
                el cambio súbito del paddingTop (se ve como si el contenido
                chocara) al parar la animación.
                */
                .animate({height: 0}, ANIM_SPEED1, function() {
                    $grid.animate({paddingTop: 0}, ANIM_SPEED2, 'linear',
                        function() {
                            $tgrid.removeClass('down');
                            // Corrige bug IE6: El paddingTop queda hasta que la sección
                            // se redibuja.
                            $grid.hide();
                        }
                    );
                });
        }
        else {
            $body.addClass(SHOW_GRID_CLASS);
            $grid
                .show()
                .animate({paddingTop: initPaddingTop}, ANIM_SPEED2, 'linear')
                .animate({height: initHeight}, ANIM_SPEED1, function() {
                    $tgrid.addClass('down');
                    // Para activar el lazy-loading de las imágenes
                    $(window).trigger('scroll');
                })
                .animate({paddingBottom: initPaddingBottom}, ANIM_SPEED2, 'linear');
        }
        return false;
    });
}

function setupPlaceholders() {
    $('input.placeholder')
        .focus(function(e) {
            var $input = $(this);
            $input.removeClass('placeholder');
            if ($input.val() == $input.attr('data-placeholder')) {
                $input.val('');
            }
        })
        .blur(function(e) {
            var $input = $(this);
            var val = $.trim($input.val());
            var placeholder = $input.attr('data-placeholder');
            if ((val === '') || (val === placeholder)) {
                $input.addClass('placeholder').val(placeholder);
            }
            else {
                $input.removeClass('placeholder');
            }
        })
        .trigger('blur');
}

$(document).ready(function() {
  setupPlaceholders();

  //$('iframe.facebook-share-button').lazyload({appear: TN.load_social_iframe});
  //$('iframe.twitter-share-button').lazyload({appear: TN.load_social_iframe});
  $('img.lzl').lazyload({'effect': 'fadeIn'});
  setupTopGrid();

  $('.fbconnect-tn').click(function(e) {
	    FB.login(function(response) {
	      if (response.session) {
	        if (response.perms) {
	          facebook_onlogin_ready();
	        }
	      }
	    }, {
	      perms : 'read_stream,publish_stream'
	    });
	    e.preventDefault();
	  });



  if ($('#twpress').length > 0) {
    TN.tn_twp = TN.tw_list('twpress', 'todonoticias', 'tn-com-ar', 310, 500);
    setTimeout('TN.tn_twp.start()', 2000);
  }

  if (($('#twmenciones').length > 0) && ( TN.twit_search != undefined) ) {
    TN.tn_src = TN.tw_search('twsearch', 310, 500, TN.twit_search);
    setTimeout('TN.tn_src.start()', 2500);
  }

  $('.jclock').jclock({format: '%H:%M'});
  


  
  $('.reac').each(function(i,e) {

	  $(e).html( $.mustache(rcbox, {box_name: 'clima'+i, box_title: 'Ideal para...'} ) );
	  var temp_desc = $('#max'+i).text() + ' ~ ' + $('#min'+i).text();

	  var cc = TN.reacciones({
	    dom: '#rcbox_clima'+i,
	    data: opc,
	    fb_desc: { name: 'Datos del clima de TN.com.ar para el 11/05',
	               description: temp_desc,
	               href: document.URL,
	               media: { type: 'image',
				src: 'http://www.ubbi.com/clima/tn/v3/grafica/grandes/28.gif',
				href: document.URL }
	             },
	    options: fix
	  });
	  cc.setup();
	  cc.builDom();
	}); 
  
  $('a.ideal-for').cluetip({
	  local: true,
	  showTitle: false,
	  sticky: true,
	  cursor: 'pointer',
	  closeText: '',
	  fx: { open: 'fadeIn', openSpeed: 'fast' },
	  cluetipClass: 'rounded'
	});
  
  $('.hd').tooltip({tipClass: 'tooltip-hd',offset: [-5,10],effect:'fade',position: 'center right'});
  
  $('body').addClass('js-finished');

});
