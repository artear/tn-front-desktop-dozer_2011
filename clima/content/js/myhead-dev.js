$LAB.script(TN.BASE_PATH + 'js/libs/jquery.js').wait()
 	.script(TN.BASE_PATH + 'js/libs/tn_fb.js')
 	.script(TN.BASE_PATH + 'js/libs/twitter-widget-min.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-lazyload.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-cluetip.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery.tools.min.js')
    .script(TN.BASE_PATH + 'js/libs/tn-reacciones.js')
    .script(TN.BASE_PATH + 'js/libs/tn-reac-cuadro.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-mustache.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-ui-min.js')
    .script(TN.BASE_PATH + 'js/plugins/jquery-jclock.js').wait()
    .script(TN.BASE_PATH + 'js/weather-zinit.js');


if (head.browser.ie)  {
	//load d_belatedpng to fix png32 in ie6
	if (parseInt(head.browser.version) <= 6)   {
		$LAB.script(TN.BASE_PATH + 'js/libs/dd_belatedpng.js',TN.BASE_PATH + 'js/libs/pngfix.js');
	}
}
